<?php
/***************************************************************************
 *                                                                          *
 *   © Simtech Development Ltd.                                             *
 *                                                                          *
 * This  is  commercial  software,  only  users  who have purchased a valid *
 * license  and  accept  to the terms of the  License Agreement can install *
 * and use this program.                                                    *
 ***************************************************************************/

use Tygh\Addons\SdPaymentTerms\ServiceProvider;
use Tygh\Addons\SdPaymentTerms\Entity\Cache as EntityCache;
use Tygh\Registry;

defined('BOOTSTRAP') or die('Access denied');

/** @var string $mode Current mode */
/** @var string $controller Current controller */

$params = $_REQUEST;

$order_id = isset($params['order_id']) ? (int) $params['order_id'] : 0;

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    return [CONTROLLER_STATUS_OK];
}

if ($mode === 'details') {
    $payment_term_service = ServiceProvider::getPaymentTermService();
    $payment_term_repository = ServiceProvider::getPaymentTermRepository();
    $payment_term_permission = ServiceProvider::getPaymentTermPermission();

    $is_user_can_view_payment_terms = $payment_term_permission->canViewPaymentTerms();
    $is_user_can_manage_payment_terms = $payment_term_permission->canManagePaymentTerms($order_id);

    $group_type = $payment_term_repository->getGroupTypeByOrderId($order_id);

    $payment_term_manager = ServiceProvider::getPaymentTermManager($order_id, $group_type);
    $allow_update_term = $payment_term_manager->allowUpdateTerm();

    if ($is_user_can_view_payment_terms && $allow_update_term->isSuccess()) {
        if ($payment_term_service->needGetTermFromSession()) {
            $allow_approve_term = false;
            $group_type = $payment_term_repository->getGroupTypeFromSession($order_id);
            $payment_term_manager = ServiceProvider::getPaymentTermManager($order_id, $group_type);
            $payment_term = $payment_term_manager->getTermFromSession();
        } else {
            $group_type = $payment_term_repository->getGroupTypeByOrderId($order_id);
            $payment_term_manager = ServiceProvider::getPaymentTermManager($order_id, $group_type);
            $payment_term = $payment_term_manager->getTermFromStorage();
        }

        $payment_term_manager->clearTermFromSession();
        $payment_term_manager->fillTermGroupStepsToSession($payment_term['groups']);

        // Need for rebuild after save payment term to the session
        if ($payment_term_service->needGetTermFromSession()) {
            $payment_term = $payment_term_manager->getTermFromSession();
            $payment_term_service->clearGetTermFromSession();
        } else {
            $payment_term = $payment_term_manager->getTermFromStorage();
        }

        $allow_approve_term = $allow_approve_term ?? $payment_term_manager->allowApproveTerm()->isSuccess();
        $payment_term_order_info = EntityCache::getOrderInfo($order_id);

        Tygh::$app['view']->assign([
            'payment_term_order_info'    => $payment_term_order_info,
            'payment_term'               => $payment_term,
            'allow_save_payment_term'    => $payment_term_manager->allowSaveTerm([])->isSuccess(),
            'allow_approve_payment_term' => $allow_approve_term,
            'allow_confirm_step_payment' => $payment_term_manager->allowConfirmStepPayment()->isSuccess()
        ]);

        Registry::set('navigation.tabs.payment_terms', [
            'title' => __('sd_payment_terms.payment_terms'),
            'js'    => true
        ]);
    }

    Tygh::$app['view']->assign([
        'is_user_can_view_payment_terms'   => $is_user_can_view_payment_terms,
        'is_user_can_manage_payment_terms' => $is_user_can_manage_payment_terms
    ]);
}
