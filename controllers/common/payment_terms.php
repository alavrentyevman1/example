<?php
/***************************************************************************
 *                                                                          *
 *   © Simtech Development Ltd.                                             *
 *                                                                          *
 * This  is  commercial  software,  only  users  who have purchased a valid *
 * license  and  accept  to the terms of the  License Agreement can install *
 * and use this program.                                                    *
 ***************************************************************************/

use Tygh\Addons\SdPaymentTerms\ServiceProvider;
use Tygh\Addons\SdPaymentTerms\Entity\Cache as EntityCache;
use Tygh\Registry;
use Tygh\Enum\ObjectStatuses;
use Tygh\Enum\SiteArea;
use Tygh\Tygh;

defined('BOOTSTRAP') or die('Access denied');

/** @var string $mode Current mode */
/** @var string $controller Current controller */

$params = $_REQUEST;

$order_id   = isset($params['order_id'])   ? (int)    $params['order_id']   : 0;
$group_id   = isset($params['group_id'])   ? (int)    $params['group_id']   : 0;
$group_type = isset($params['group_type']) ? (string) $params['group_type'] : '';
$step_id    = isset($params['step_id'])    ? (int)    $params['step_id']    : 0;

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    if (in_array($mode, [
        'change_group_type',
        'save_group',
        'delete_group',
        'save_step',
        'delete_step'
    ])) {
        if (!$order_id || !defined('AJAX_REQUEST')) {
            return;
        }

        $payment_term_manager = ServiceProvider::getPaymentTermManager($order_id, $group_type);
        $allow_update_term = $payment_term_manager->allowUpdateTerm();

        if ($allow_update_term->isFailure()) {
            $allow_update_term->showNotifications();

            return [CONTROLLER_STATUS_DENIED];
        }

        switch ($mode) {
            case 'change_group_type':
                $payment_term_manager->changeTermGroupTypeFromSession();
                break;
            case 'save_group':
                $payment_term_manager->saveTermGroupToSession($group_id, $params);
                break;
            case 'delete_group':
                $payment_term_manager->deleteTermGroupFromSession($group_id);
                break;
            case 'save_step':
                $payment_term_manager->saveTermStepToSession($group_id, $step_id, $params);
                break;
            case 'delete_step':
                $payment_term_manager->deleteTermStepFromSession($group_id, $step_id);
                break;
        }

        $payment_term_order_info = EntityCache::getOrderInfo($order_id);
        $payment_term_permission = ServiceProvider::getPaymentTermPermission();
        $payment_term = $payment_term_manager->getTermFromSession([
            'group_type' => $group_type
        ]);

        Tygh::$app['view']->assign([
            'payment_term_order_info'             => $payment_term_order_info,
            'payment_term'                        => $payment_term,
            'is_user_can_view_payment_terms'      => $payment_term_permission->canViewPaymentTerms(),
            'is_user_can_manage_payment_terms'    => $payment_term_permission->canManagePaymentTerms($order_id),
            'allow_save_payment_term'             => $payment_term_manager->allowSaveTerm([])->isSuccess(),
            'allow_approve_payment_term'          => false,
            'allow_confirm_step_payment'          => false,
            'show_need_save_payment_terms_notice' => true
        ]);

        Tygh::$app['view']->display('backend:addons/sd_payment_terms/views/orders/components/payment_term/group_types.tpl');

        return [CONTROLLER_STATUS_NO_CONTENT];
    }

    if ($mode === 'save') {
        if (!$order_id) {
            return;
        }

        $payment_term_service = ServiceProvider::getPaymentTermService();
        $group_type = ServiceProvider::getPaymentTermRepository()->getGroupTypeFromSession($order_id);
        $group_list = ServiceProvider::getPaymentTermRepository()->getFromSession($order_id);

        $payment_term_manager = ServiceProvider::getPaymentTermManager($order_id, $group_type);
        $allow_save_term = $payment_term_manager->allowSaveTerm($group_list);

        if ($allow_save_term->isFailure()) {
            $allow_save_term->showNotifications();
            $payment_term_service->setGetTermFromSession();
        } else {
            $term_saved = $payment_term_manager->saveTerm($group_list);

            if ($term_saved) {
                $payment_term_manager->clearApproveTerm();
                $payment_term_manager->approveTerm();
            }
        }

        return [CONTROLLER_STATUS_OK, "orders.details&order_id={$order_id}&selected_section=payment_terms"];
    }

    if ($mode === 'approve') {
        if (!$order_id) {
            return;
        }

        $payment_term_service = ServiceProvider::getPaymentTermService();
        $group_type = ServiceProvider::getPaymentTermRepository()->getGroupTypeByOrderId($order_id);
        $payment_term_manager = ServiceProvider::getPaymentTermManager($order_id, $group_type);
        $allow_approve_term = $payment_term_manager->allowApproveTerm();

        if ($allow_approve_term->isFailure()) {
            $allow_approve_term->showNotifications();
        } else {
            $payment_term_manager->approveTerm();
        }

        return [CONTROLLER_STATUS_OK, "orders.details&order_id={$order_id}&selected_section=payment_terms"];
    }

    if ($mode === 'confirm_step_payment') {
        if (!$order_id || !$group_id || !$step_id) {
            return;
        }

        $group_type = ServiceProvider::getPaymentTermRepository()->getGroupTypeByOrderId($order_id);
        $payment_term_manager = ServiceProvider::getPaymentTermManager($order_id, $group_type);
        $allow_confirm_step_payment = $payment_term_manager->allowConfirmStepPayment();

        if ($allow_confirm_step_payment->isFailure()) {
            $allow_confirm_step_payment->showNotifications();
        } else {
            $payment_term_manager->confirmStepPayment($group_id, $step_id);
            ServiceProvider::getPaymentTermSync()->syncOrderPaymentTotalConfirmed($order_id);
        }

        return [CONTROLLER_STATUS_OK, "orders.details&order_id={$order_id}&selected_section=payment_terms"];
    }

    return [CONTROLLER_STATUS_OK];
}
