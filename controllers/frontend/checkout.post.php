<?php
/***************************************************************************
 *                                                                          *
 *   © Simtech Development Ltd.                                             *
 *                                                                          *
 * This  is  commercial  software,  only  users  who have purchased a valid *
 * license  and  accept  to the terms of the  License Agreement can install *
 * and use this program.                                                    *
 ***************************************************************************/

use Tygh\Addons\SdPaymentTerms\ServiceProvider;
use Tygh\Addons\SdPaymentTerms\Enum\PaymentTermGroupTypes;

defined('BOOTSTRAP') or die('Access denied');

/** @var string $mode Current mode */
/** @var string $controller Current controller */

$params = $_REQUEST;

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    return [CONTROLLER_STATUS_OK];
}

if ($mode === 'checkout') {
    $payment_term_manager = ServiceProvider::getPaymentTermManager(0, PaymentTermGroupTypes::getDefault());
    $allow_create_term = $payment_term_manager->allowCreateTerm();

    if ($allow_create_term->isSuccess()) {
        Tygh::$app['view']->assign('is_payment_term', true);
    }
}
