<?php
/***************************************************************************
 *                                                                          *
 *   © Simtech Development Ltd.                                             *
 *                                                                          *
 * This  is  commercial  software,  only  users  who have purchased a valid *
 * license  and  accept  to the terms of the  License Agreement can install *
 * and use this program.                                                    *
 ***************************************************************************/

use Tygh\Addons\SdPaymentTerms\ServiceProvider;
use Tygh\Registry;

defined('BOOTSTRAP') or die('Access denied');

/** @var string $mode Current mode */
/** @var string $controller Current controller */

$params = $_REQUEST;

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    return [CONTROLLER_STATUS_OK];
}

if (in_array($mode, ['cron_notifications', 'cron_triggers'])) {
    $cron_password = $params['cron_password'] ?? '';

    if ($cron_password && $cron_password === Registry::get('addons.sd_payment_terms.cron_password')) {
        switch ($mode) {
            case 'cron_notifications':
                ServiceProvider::getPaymentTermService()->runCronNotifications();
                break;
            case 'cron_triggers':
                ServiceProvider::getPaymentTermService()->runCronTriggers();
                break;
        }

        echo('Done');
    } else {
        echo('Access denied');
    }

    exit;
}
