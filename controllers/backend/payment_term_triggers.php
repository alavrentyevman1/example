<?php
/***************************************************************************
 *                                                                          *
 *   © Simtech Development Ltd.                                             *
 *                                                                          *
 * This  is  commercial  software,  only  users  who have purchased a valid *
 * license  and  accept  to the terms of the  License Agreement can install *
 * and use this program.                                                    *
 ***************************************************************************/

use Tygh\Addons\SdPaymentTerms\ServiceProvider;
use Tygh\Addons\SdPaymentTerms\Models\PaymentTermTrigger;
use Tygh\Registry;
use Tygh\Addons\SdShippingMethods\Enum\TransportHubTypes;

defined('BOOTSTRAP') or die('Access denied');

/** @var string $mode Current mode */
/** @var string $controller Current controller */

$params = $_REQUEST;
$trigger_id = isset($params['trigger_id']) ? (int) $params['trigger_id'] : 0;

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $suffix = 'manage';

    if ($mode === 'update') {
        $trigger = $trigger_id ? PaymentTermTrigger::model()->find($trigger_id) : new PaymentTermTrigger;
        $trigger->attributes($params['trigger_data']);
        $trigger->save();

        if (empty($trigger->trigger_id)) {
            $suffix = 'manage';
        } else {
            $suffix = 'update?trigger_id=' . $trigger->trigger_id;
        }
    }

    if (
        $mode === 'm_update_statuses'
        && !empty($params['trigger_ids'])
        && !empty($params['status'])
    ) {
        $triggers = PaymentTermTrigger::model()->findMany(['ids' => (array) $params['trigger_ids']]);

        foreach ($triggers as $trigger) {
            $trigger->status = $params['status'];
            $trigger->save();
        }
    }

    return [CONTROLLER_STATUS_OK, 'payment_term_triggers.' . $suffix];
}

if ($mode === 'add') {
    return [CONTROLLER_STATUS_NO_PAGE];
}

if ($mode === 'update') {
    $trigger = PaymentTermTrigger::model()->find($trigger_id);

    if (!$trigger) {
        return [CONTROLLER_STATUS_NO_PAGE];
    }

    $tabs = [
        'general' => [
            'title' => __('general'),
            'js'    => true
        ]
    ];

    Registry::set('navigation.tabs', $tabs);

    Tygh::$app['view']->assign([
        'trigger'        => $trigger,
        'transport_hubs' => TransportHubTypes::getAllWithNames()
    ]);
}

if ($mode === 'manage') {
    $params = array_merge([
        'load_triggers'  => true,
        'items_per_page' => Registry::get('settings.Appearance.admin_elements_per_page'),
        'return_params'  => true
    ], $params);

    [$triggers, $search] = PaymentTermTrigger::model()->findMany($params);

    Tygh::$app['view']->assign([
        'triggers' => $triggers,
        'search'   => $search
    ]);
}

if (in_array($mode, ['update', 'manage'])) {
    ServiceProvider::getPaymentsService()->setNavigationDynamicSections($controller);
}
