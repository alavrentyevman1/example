<?php
/***************************************************************************
 *                                                                          *
 *   © Simtech Development Ltd.                                             *
 *                                                                          *
 * This  is  commercial  software,  only  users  who have purchased a valid *
 * license  and  accept  to the terms of the  License Agreement can install *
 * and use this program.                                                    *
 ***************************************************************************/

use Tygh\Addons\SdPaymentTerms\ServiceProvider;

defined('BOOTSTRAP') or die('Access denied');

/** @var string $mode Current mode */
/** @var string $controller Current controller */

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    return [CONTROLLER_STATUS_OK];
}

if ($mode === 'manage') {
    ServiceProvider::getPaymentsService()->setNavigationDynamicSections($controller);
}
