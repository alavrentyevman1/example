<?php
/***************************************************************************
 *                                                                          *
 *   © Simtech Development Ltd.                                             *
 *                                                                          *
 * This  is  commercial  software,  only  users  who have purchased a valid *
 * license  and  accept  to the terms of the  License Agreement can install *
 * and use this program.                                                    *
 ***************************************************************************/

namespace Tygh\Addons\SdPaymentTerms\Enum;

class PaymentTermStatuses
{
    const EMPTY            = '';
    const ADMIN_APPROVE    = 'A';
    const CUSTOMER_APPROVE = 'C';
    const FULL_APPROVE     = 'F';

    public static function getAll(): array
    {
        return [
            self::ADMIN_APPROVE,
            self::CUSTOMER_APPROVE,
            self::FULL_APPROVE
        ];
    }
}
