<?php
/***************************************************************************
 *                                                                          *
 *   © Simtech Development Ltd.                                             *
 *                                                                          *
 * This  is  commercial  software,  only  users  who have purchased a valid *
 * license  and  accept  to the terms of the  License Agreement can install *
 * and use this program.                                                    *
 ***************************************************************************/

namespace Tygh\Addons\SdPaymentTerms\Enum;

class PaymentTermNotificationTypes
{
    const EMPTY              = '';
    const BEFORE_END_PAYMENT = 'B';
    const TODAY_END_PAYMENT  = 'T';
    const AFTER_END_PAYMENT  = 'A';

    public static function getAll(): array
    {
        return [
            self::BEFORE_END_PAYMENT,
            self::TODAY_END_PAYMENT,
            self::AFTER_END_PAYMENT
        ];
    }
}
