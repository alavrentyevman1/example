<?php
/***************************************************************************
 *                                                                          *
 *   © Simtech Development Ltd.                                             *
 *                                                                          *
 * This  is  commercial  software,  only  users  who have purchased a valid *
 * license  and  accept  to the terms of the  License Agreement can install *
 * and use this program.                                                    *
 ***************************************************************************/

namespace Tygh\Addons\SdPaymentTerms\Enum;

class PaymentTermGroupTypes
{
    const SINGLE    = 'S';
    const PRODUCTS  = 'P';
    const SHIPMENTS = 'H';

    public static function getDefault(): string
    {
        return self::SINGLE;
    }

    public static function getAll(): array
    {
        return [
            self::SINGLE,
            self::PRODUCTS,
            self::SHIPMENTS
        ];
    }

    public static function isExist(string $group_type): bool
    {
        return in_array($group_type, self::getAll());
    }
}
