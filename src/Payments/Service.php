<?php
/***************************************************************************
 *                                                                          *
 *   © Simtech Development Ltd.                                             *
 *                                                                          *
 * This  is  commercial  software,  only  users  who have purchased a valid *
 * license  and  accept  to the terms of the  License Agreement can install *
 * and use this program.                                                    *
 ***************************************************************************/

namespace Tygh\Addons\SdPaymentTerms\Payments;

use Tygh\Registry;

class Service
{
    public function setNavigationDynamicSections(string $controller): void
    {
        $dynamic_sections = Registry::ifGet('navigation.dynamic.sections', []);

        $dynamic_sections['payments'] = [
            'title' => __('payment_methods'),
            'href'  => 'payments.manage'
        ];

        $dynamic_sections['payment_term_triggers'] = [
            'title' => __('sd_payment_terms.payment_terms'),
            'href'  => 'payment_term_triggers.manage'
        ];

        Registry::set('navigation.dynamic.active_section', $controller);
        Registry::set('navigation.dynamic.sections', $dynamic_sections);
    }

    public function getFirstAccountPaymentId(): int
    {
        $processors = fn_get_schema('rus_payments', 'processors', 'php');
        $processor_script = $processors['account']['processor_script'] ?? '';

        if (empty($processor_script)) {
            return 0;
        }

        $payments = fn_get_payments([
            'processor_script' => $processor_script
        ]);

        if (!$payments) {
            return 0;
        }

        $first_payment = reset($payments);

        return $first_payment['payment_id'] ?? 0;
    }
}
