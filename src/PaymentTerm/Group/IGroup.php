<?php
/***************************************************************************
 *                                                                          *
 *   © Simtech Development Ltd.                                             *
 *                                                                          *
 * This  is  commercial  software,  only  users  who have purchased a valid *
 * license  and  accept  to the terms of the  License Agreement can install *
 * and use this program.                                                    *
 ***************************************************************************/

namespace Tygh\Addons\SdPaymentTerms\PaymentTerm\Group;

interface IGroup
{
    const SESSION_PAYMENT_TERM_GROUPS_FIELD = 'payment_term_groups';

    public function getGroupType(): string;

    public function setGroupIdBySession(array $session): IGroup;

    public function getGroupId(): int;

    public function setGroupTotals(): void;

    public function setSteps(array $steps, array $extra_methods = []): IGroup;

    public function getSteps(): array;

    public function haveProducts(): bool;

    public function toArray(): array;

    public function saveToSession(array $session): array;

    public function deleteFromSession(array $session): array;

    public function saveToStorage(array $group_data = []): bool;

    public function deleteFromStorage(): bool;

    public function setPaymentData(): void;
}
