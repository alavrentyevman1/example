<?php
/***************************************************************************
 *                                                                          *
 *   © Simtech Development Ltd.                                             *
 *                                                                          *
 * This  is  commercial  software,  only  users  who have purchased a valid *
 * license  and  accept  to the terms of the  License Agreement can install *
 * and use this program.                                                    *
 ***************************************************************************/

namespace Tygh\Addons\SdPaymentTerms\PaymentTerm\Group\Groups;

use Tygh\Addons\SdPaymentTerms\ServiceProvider;
use Tygh\Addons\SdPaymentTerms\PaymentTerm\Group\AGroup;
use Tygh\Addons\SdPaymentTerms\Entity\Cache as EntityCache;
use Tygh\Addons\SdPaymentTerms\Enum\PaymentTermGroupTypes;

class ProductsGroup extends AGroup
{
    protected $products = [];

    public function __construct(array $group_data)
    {
        parent::__construct($group_data);

        $this->products = isset($group_data['products']) ? (array) $group_data['products'] : [];
        $this->products = $this->prepareProducts($this->products);
        $this->products = $this->filterProducts($this->products);
    }

    public function getGroupType(): string
    {
        return PaymentTermGroupTypes::PRODUCTS;
    }

    public function haveProducts(): bool
    {
        return true;
    }

    public function toArray(): array
    {
        $group_data = parent::toArray();
        $group_data['products'] = $this->products;

        return $group_data;
    }

    protected function prepareProducts(array $product_list): array
    {
        return array_map(function ($product_data) {
            return [
                'item_id' => isset($product_data['item_id']) ? (int) $product_data['item_id'] : 0,
                'amount'  => isset($product_data['amount'])  ? (int) $product_data['amount']  : 0,
            ];
        }, $product_list);
    }

    private function filterProducts(array $product_list): array
    {
        $order_info = EntityCache::getOrderInfo($this->order_id);

        return array_filter($product_list, function ($product) use ($order_info) {
            return
                !empty($product['item_id'])
                && !empty($product['amount'])
                && !empty($order_info['products'][$product['item_id']])
                && empty($order_info['products'][$product['item_id']]['extra']['exclude_from_calculate']);
        });
    }

    public function gatherAdditionalProducts(): AGroup
    {
        $order_info = EntityCache::getOrderInfo($this->order_id);

        $this->products = array_map(function ($product) use ($order_info) {
            $product['price'] = $product['subtotal'] = 0;
            $order_product = $order_info['products'][$product['item_id']] ?? [];

            if ($order_product) {
                $product['price'] = $order_product['price'];
            }

            $product['subtotal'] = $product['price'] * $product['amount'];

            return $product;
        }, $this->products);

        return $this;
    }

    public function setGroupTotals(): void
    {
        $order_info = EntityCache::getOrderInfo($this->order_id);

        if (!$order_info) {
            return;
        }

        parent::setGroupTotals();

        if (empty(reset($this->products)['subtotal'])) {
            $this->gatherAdditionalProducts();
        }

        $repository = ServiceProvider::getPaymentTermRepository();
        $group_ids = $repository->getGroupIdsFromSession($this->order_id) ?: $repository->getGroupIdsFromStorage($this->order_id);

        $order_total_diff = $order_info['total'] - $order_info['subtotal'] - $order_info['shipping_cost'];
        $discount = $this->divideGroupTotal($group_ids, -$order_total_diff);
        $this->group_totals['shipping_cost'] = $this->divideGroupTotal($group_ids, $order_info['shipping_cost']);
        $this->group_totals['subtotal_discount'] = $this->divideGroupTotal($group_ids, $this->group_totals['subtotal_discount']);

        foreach ($this->group_totals['taxes'] as &$tax) {
            $tax['tax_subtotal'] = $this->divideGroupTotal($group_ids, $tax['tax_subtotal']);
        }

        $this->group_totals['subtotal'] = array_sum(
            array_column($this->products, 'subtotal')
        );
        $this->group_totals['total'] = $this->group_totals['subtotal'] + $this->group_totals['shipping_cost'] - $discount;
    }
}
