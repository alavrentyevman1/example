<?php
/***************************************************************************
 *                                                                          *
 *   © Simtech Development Ltd.                                             *
 *                                                                          *
 * This  is  commercial  software,  only  users  who have purchased a valid *
 * license  and  accept  to the terms of the  License Agreement can install *
 * and use this program.                                                    *
 ***************************************************************************/

namespace Tygh\Addons\SdPaymentTerms\PaymentTerm\Group\Groups;

use Tygh\Addons\SdPaymentTerms\PaymentTerm\Group\AGroup;
use Tygh\Addons\SdPaymentTerms\Enum\PaymentTermGroupTypes;

class SingleGroup extends AGroup
{
    public function getGroupType(): string
    {
        return PaymentTermGroupTypes::SINGLE;
    }

    public function haveProducts(): bool
    {
        return false;
    }
}
