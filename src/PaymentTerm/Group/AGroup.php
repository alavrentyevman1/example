<?php
/***************************************************************************
 *                                                                          *
 *   © Simtech Development Ltd.                                             *
 *                                                                          *
 * This  is  commercial  software,  only  users  who have purchased a valid *
 * license  and  accept  to the terms of the  License Agreement can install *
 * and use this program.                                                    *
 ***************************************************************************/

namespace Tygh\Addons\SdPaymentTerms\PaymentTerm\Group;

use Tygh\Addons\SdPaymentTerms\ServiceProvider;
use Tygh\Addons\SdPaymentTerms\Entity\Cache as EntityCache;
use Tygh\Addons\SdPaymentTerms\Entity\CommonTrait as EntityCommonTrait;

abstract class AGroup implements IGroup
{
    use EntityCommonTrait;

    protected $group_type;

    protected $group_id;

    protected $order_id;

    protected $steps = [];

    protected $group_totals = [];

    protected $payment_data = [];

    protected $fillable = [
        'order_id',
        'group_id',
        'group_type',
        'products'
    ];

    public function __construct(array $group_data)
    {
        $this->group_id = isset($group_data['group_id']) ? (int) $group_data['group_id'] : 0;
        $this->order_id = isset($group_data['order_id']) ? (int) $group_data['order_id'] : 0;

        $this->group_type = $this->getGroupType();
    }

    public function setGroupIdBySession(array $session): IGroup
    {
        if (!$this->group_id) {
            $groups_count = array_key_last($session);
            $this->group_id = ++$groups_count;
        }

        return $this;
    }

    public function getGroupId(): int
    {
        return (int) $this->group_id;
    }

    public function setGroupTotals(): void
    {
        $order_info = EntityCache::getOrderInfo($this->order_id);

        if ($order_info) {
            $this->group_totals = [
                'total'             => $order_info['total'],
                'shipping_cost'     => $order_info['shipping_cost'],
                'subtotal_discount' => $order_info['subtotal_discount'],
                'taxes'             => $order_info['taxes']
            ];
        }
    }

    public function setSteps(array $steps, array $extra_methods = []): IGroup
    {
        $factory = ServiceProvider::getPaymentTermFactory();

        array_walk($steps, function ($step_data) use ($factory, $extra_methods) {
            $this->steps[] = $factory->createStep($step_data['group_type'], $step_data, $extra_methods);
        });

        return $this;
    }

    public function getSteps(): array
    {
        return $this->steps;
    }

    public function toArray(): array
    {
        $steps = array_map(function ($step) {
            return $step->toArray();
        }, $this->steps);

        return [
            'group_id'     => $this->group_id,
            'group_type'   => $this->group_type,
            'order_id'     => $this->order_id,
            'steps'        => $steps,
            'group_totals' => $this->group_totals,
            'payment_data' => $this->payment_data
        ];
    }

    public function saveToSession(array $session): array
    {
        if (!$this->group_id) {
            return $session;
        }

        $step_data = $this->toArray();
        $step_data = $this->filterFillableFields($step_data);
        unset($step_data['steps']);

        if (empty($session[$this->group_id])) {
            $session[$this->group_id] = $step_data;
        } else {
            $session[$this->group_id] = array_merge(
                $session[$this->group_id],
                $step_data
            );
        }

        return $session;
    }

    public function deleteFromSession(array $session): array
    {
        unset($session[$this->group_id]);

        return $session;
    }

    public function saveToStorage(array $group_data = []): bool
    {
        $storage_data = $this->toArray();
        $storage_data = $this->filterFillableFields($storage_data);
        $storage_data = array_map(function ($value) {
            return is_array($value) ? serialize($value) : $value;
        }, $storage_data);

        return (bool) db_replace_into(
            'order_payment_term_groups',
            array_merge($group_data, $storage_data)
        );
    }

    public function deleteFromStorage(): bool
    {
        return (bool) db_query('DELETE FROM ?:order_payment_term_groups WHERE ?w', [
            'order_id' => $this->order_id,
            'group_id' => $this->group_id
        ]);
    }

    public function setPaymentData(): void
    {
        if (!$this->group_totals) {
            $this->setGroupTotals();
        }

        $payment_data = [
            'confirmed'               => false,
            'total'                   => $this->group_totals['total'] ?? 0,
            'total_confirmed'         => 0,
            'total_confirmed_percent' => 0
        ];

        foreach ($this->steps as $step) {
            $step_data = $step->toArray();

            if (!empty($step_data['payment_data']['confirmed'])) {
                $payment_data['total_confirmed'] += $step_data['absolute_amount'];
            }
        }

        if ($payment_data['total_confirmed'] > 0) {
            $service = ServiceProvider::getPaymentTermService();

            $payment_data['total_confirmed_percent'] = $service->calculatePercentValue(
                $payment_data['total_confirmed'],
                $payment_data['total']
            );
            $payment_data['confirmed'] = $payment_data['total_confirmed_percent'] === $service::FULL_PERCENT_STACK;
        }

        $this->payment_data = $payment_data;
    }

    protected function divideGroupTotal(array $group_ids, float $total): float
    {
        if (!in_array($this->group_id, $group_ids)) {
            return (float) 0;
        }

        $group_count = count($group_ids);
        $total_per_iteration = $total / count($group_ids);

        if ($group_count === 1) {
            $divided_total = fn_format_price($total);
        } elseif ($this->group_id === end($group_ids)) {
            $previous_groups_total = --$group_count * fn_format_price($total_per_iteration);
            $divided_total = fn_format_price($total - $previous_groups_total);
        } else {
            $divided_total = fn_format_price($total_per_iteration);
        }

        return $divided_total;
    }
}
