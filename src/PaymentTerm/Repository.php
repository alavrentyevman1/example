<?php
/***************************************************************************
 *                                                                          *
 *   © Simtech Development Ltd.                                             *
 *                                                                          *
 * This  is  commercial  software,  only  users  who have purchased a valid *
 * license  and  accept  to the terms of the  License Agreement can install *
 * and use this program.                                                    *
 ***************************************************************************/

namespace Tygh\Addons\SdPaymentTerms\PaymentTerm;

use Tygh\Addons\SdPaymentTerms\ServiceProvider;
use Tygh\Addons\SdPaymentTerms\PaymentTerm\Group\IGroup;
use Tygh\Tygh;

class Repository
{
    public function &getFromSession(int $order_id): array
    {
        if (empty(Tygh::$app['session'][IGroup::SESSION_PAYMENT_TERM_GROUPS_FIELD])) {
            Tygh::$app['session'][IGroup::SESSION_PAYMENT_TERM_GROUPS_FIELD] = [];
        }

        if (empty(Tygh::$app['session'][IGroup::SESSION_PAYMENT_TERM_GROUPS_FIELD][$order_id])) {
            Tygh::$app['session'][IGroup::SESSION_PAYMENT_TERM_GROUPS_FIELD][$order_id] = [];
        }

        return Tygh::$app['session'][IGroup::SESSION_PAYMENT_TERM_GROUPS_FIELD][$order_id];
    }

    public function getGroupFromSession(int $order_id, int $group_id): array
    {
        return $this->getFromSession($order_id)[$group_id] ?? [];
    }

    public function getFromStorage(int $order_id): array
    {
        if (!$order_id) {
            return [];
        }

        $group_list = db_get_array('SELECT * FROM ?:order_payment_term_groups WHERE order_id = ?i', $order_id);
        $step_list = db_get_hash_multi_array('SELECT * FROM ?:order_payment_term_steps WHERE order_id = ?i', ['group_id', 'step_id'], $order_id);

        $group_list = array_map(function ($group_data) use ($step_list) {
            foreach ($group_data as &$value) {
                if ($value === 'b:0;' || @unserialize($value) !== false) {
                    $value = unserialize($value);
                }
            }

            $group_data['steps'] = $step_list[$group_data['group_id']] ?? [];

            return $group_data;
        }, $group_list);

        return $group_list;
    }

    public function getGroupTypeFromSession(int $order_id): string
    {
        $group_list = $this->getFromSession($order_id);
        $group_type = $group_list ? reset($group_list)['group_type'] : '';

        return $group_type;
    }

    public function getGroupTypeByOrderId(int $order_id): string
    {
        return $order_id
            ? db_get_field('SELECT group_type FROM ?:order_payment_term_groups WHERE order_id = ?i', $order_id)
            : '';
    }

    public function getGroupIdsFromSession(int $order_id): array
    {
        $group_ids = array_column($this->getFromSession($order_id), 'group_id');
        asort($group_ids);

        return $group_ids;
    }

    public function getGroupIdsFromStorage(int $order_id): array
    {
        if ($order_id) {
            $group_ids = db_get_fields('SELECT group_id FROM ?:order_payment_term_groups WHERE order_id = ?i', $order_id);
            asort($group_ids);
        }

        return $group_ids ?? [];
    }

    public function findStepsFromStorage(int $order_id, array $params = []): array
    {
        if (!$order_id) {
            return [];
        }

        $conditions = [
            'order_id' => $order_id
        ];

        if (!empty($params['trigger_ids'])) {
            $conditions['trigger_id'] = (array) $params['trigger_ids'];
        }

        if (!empty($params['group_id']) && !empty($params['step_ids'])) {
            $conditions['group_id'] = $params['group_id'];
            $conditions['step_id'] = (array) $params['step_ids'];
        }

        $steps = db_get_array('SELECT * FROM ?:order_payment_term_steps WHERE ?w', $conditions);

        if (empty($params['to_array'])) {
            $factory = ServiceProvider::getPaymentTermFactory();

            $steps = array_map(function ($step_data) use ($factory) {
                return $factory->createStep($step_data['group_type'], $step_data);
            }, $steps);
        }

        return $steps;
    }

    public function getOrderPaymentTermStatus(int $order_id): string
    {
        return $order_id
            ? (string) db_get_field('SELECT payment_term_status FROM ?:orders WHERE order_id = ?i', $order_id)
            : '';
    }

    public function isExistsByOrderId(int $order_id): bool
    {
        return !empty($this->getGroupIdsFromStorage($order_id));
    }

    public function getEmptyEndPaymentTimestampOrderIds(): array
    {
        return db_get_fields('SELECT DISTINCT order_id FROM ?:order_payment_term_steps WHERE end_payment_timestamp = ?i', 0);
    }
}
