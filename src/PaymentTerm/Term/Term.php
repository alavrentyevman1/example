<?php
/***************************************************************************
 *                                                                          *
 *   © Simtech Development Ltd.                                             *
 *                                                                          *
 * This  is  commercial  software,  only  users  who have purchased a valid *
 * license  and  accept  to the terms of the  License Agreement can install *
 * and use this program.                                                    *
 ***************************************************************************/

namespace Tygh\Addons\SdPaymentTerms\PaymentTerm\Term;

use Tygh\Addons\SdPaymentTerms\ServiceProvider;
use Tygh\Addons\SdPaymentTerms\PaymentTerm\StatusProperties;
use Tygh\Addons\SdPaymentTerms\Enum\PaymentTermStatuses;

class Term
{
    public $order_id;

    public $groups = [];

    public $group_properties = [];

    public $triggers = [];

    public $group_type;

    public $status_properties = [];

    public $payment_data = [];

    public function toArray(): array
    {
        $groups = array_map(function ($group) {
            return $group->toArray();
        }, $this->groups);

        return [
            'order_id'          => $this->order_id,
            'groups'            => $groups,
            'group_properties'  => $this->group_properties,
            'triggers'          => $this->triggers,
            'group_type'        => $this->group_type,
            'status_properties' => $this->status_properties,
            'payment_data'      => $this->payment_data
        ];
    }

    public function approve(): bool
    {
        $repository = ServiceProvider::getPaymentTermRepository();

        if (!$repository->isExistsByOrderId($this->order_id)) {
            return false;
        }

        $status = $repository->getOrderPaymentTermStatus($this->order_id);

        if (StatusProperties::isApproved($status)) {
            return false;
        }

        $approve_status = StatusProperties::getApproveStatusBySiteArea();

        if (StatusProperties::isFullApprove($status)) {
            $approve_status = PaymentTermStatuses::FULL_APPROVE;
        }

        return (bool) db_query('UPDATE ?:orders SET payment_term_status = ?s WHERE order_id = ?i', $approve_status, $this->order_id);
    }

    public function clearApprove(): bool
    {
        return (bool) db_query('UPDATE ?:orders SET payment_term_status = ?s WHERE order_id = ?i', '', $this->order_id);
    }
}
