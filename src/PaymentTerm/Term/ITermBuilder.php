<?php
/***************************************************************************
 *                                                                          *
 *   © Simtech Development Ltd.                                             *
 *                                                                          *
 * This  is  commercial  software,  only  users  who have purchased a valid *
 * license  and  accept  to the terms of the  License Agreement can install *
 * and use this program.                                                    *
 ***************************************************************************/

namespace Tygh\Addons\SdPaymentTerms\PaymentTerm\Term;

interface ITermBuilder
{
    public function create(): ITermBuilder;

    public function setOrderId(int $order_id): ITermBuilder;

    public function setGroupsWithSteps(array $group_list): ITermBuilder;

    public function withGroupProperties(): ITermBuilder;

    public function withTriggers(bool $filter_terms = true): ITermBuilder;

    public function withGroupType(string $default_group_type = ''): ITermBuilder;

    public function withStatusProperties(): ITermBuilder;

    public function withPaymentData(): ITermBuilder;

    public function toArray(): ITermBuilder;

    public function getTerm();
}
