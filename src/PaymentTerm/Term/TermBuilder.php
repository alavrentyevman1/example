<?php
/***************************************************************************
 *                                                                          *
 *   © Simtech Development Ltd.                                             *
 *                                                                          *
 * This  is  commercial  software,  only  users  who have purchased a valid *
 * license  and  accept  to the terms of the  License Agreement can install *
 * and use this program.                                                    *
 ***************************************************************************/

namespace Tygh\Addons\SdPaymentTerms\PaymentTerm\Term;

use Tygh\Addons\SdPaymentTerms\ServiceProvider;
use Tygh\Addons\SdPaymentTerms\PaymentTerm\Factory;
use Tygh\Addons\SdPaymentTerms\PaymentTerm\Repository;
use Tygh\Addons\SdPaymentTerms\Models\PaymentTermTrigger;
use Tygh\Addons\SdPaymentTerms\PaymentTerm\StatusProperties;
use Tygh\Addons\SdPaymentTerms\Entity\Cache as EntityCache;
use Tygh\Addons\SdPaymentTerms\Enum\PaymentTermGroupTypes;
use Tygh\Addons\SdShippingMethods\ServiceProvider as ShippingMethodsServiceProvider;
use Tygh\Addons\SdShippingMethods\Enum\TransportHubTypes;
use Tygh\Enum\ObjectStatuses;
use Tygh\Exceptions\DeveloperException;

class TermBuilder implements ITermBuilder
{
    protected $factory;

    protected $repository;

    private $term;

    public function __construct(Factory $factory, Repository $repository)
    {
        $this->factory = $factory;
        $this->repository = $repository;

        $this->create();
    }

    public function create(): ITermBuilder
    {
        $this->term = new Term();

        return $this;
    }

    public function setOrderId(int $order_id): ITermBuilder
    {
        $this->term->order_id = $order_id;

        return $this;
    }

    public function setGroupsWithSteps(array $group_list): ITermBuilder
    {
        $group_list = array_map(function ($group) {
            if (!empty($group['steps'])) {
                array_multisort(
                    array_column($group['steps'], 'trigger_id'),
                    SORT_ASC,
                    $group['steps']
                );
            }

            return $group;
        }, $group_list);

        array_walk($group_list, function ($group_data) {
            $group = $this->factory->createGroup($group_data['group_type'], $group_data, [
                'gatherAdditionalProducts',
                'setGroupTotals'
            ]);

            if (!empty($group_data['steps'])) {
                $group->setSteps($group_data['steps'], [
                    'setTriggerName',
                    'setAbsoluteAmount',
                    'setPaymentData'
                ]);
            }

            $group->setPaymentData();

            $this->term->groups[] = $group;
        });

        return $this;
    }

    public function withGroupProperties(): ITermBuilder
    {
        $this->term->group_properties = fn_get_schema('sd_payment_terms', 'payment_group_properties');

        return $this;
    }

    public function withTriggers(bool $filter_terms = true): ITermBuilder
    {
        $this->term->triggers = PaymentTermTrigger::model()->findAll([
            'to_array' => true,
            'status'   => ObjectStatuses::ACTIVE
        ]);

        if ($filter_terms) {
            $this->filterTerms();
        }

        return $this;
    }

    public function withGroupType(string $default_group_type = ''): ITermBuilder
    {
        if ($this->term->groups) {
            $group = reset($this->term->groups);
            $this->term->group_type = $group->getGroupType();
        }

        if (!$this->term->group_type) {
            $this->term->group_type = $default_group_type ?: PaymentTermGroupTypes::getDefault();
        }

        return $this;
    }

    public function withStatusProperties(): ITermBuilder
    {
        if (!$this->term->order_id) {
            throw new DeveloperException('You need to call setOrderId() method before call withStatus() method');
        }

        $status = $this->repository->getOrderPaymentTermStatus($this->term->order_id);
        $this->term->status_properties = StatusProperties::getStatusProperties($status);

        return $this;
    }

    public function withPaymentData(): ITermBuilder
    {
        if (!$this->term->order_id) {
            throw new DeveloperException('You need to call setOrderId() method before call withPaymentData() method');
        }

        $order_info = EntityCache::getOrderInfo($this->term->order_id);

        $payment_data = [
            'confirmed'               => false,
            'total'                   => $order_info['total'] ?? 0,
            'total_confirmed'         => 0,
            'total_confirmed_percent' => 0
        ];

        foreach ($this->term->groups as $group) {
            $group_data = $group->toArray();

            if (!empty($group_data['payment_data']['total_confirmed'])) {
                $payment_data['total_confirmed'] += $group_data['payment_data']['total_confirmed'];
            }
        }

        if ($payment_data['total_confirmed'] > 0) {
            $service = ServiceProvider::getPaymentTermService();

            $payment_data['total_confirmed_percent'] = $service->calculatePercentValue(
                $payment_data['total_confirmed'],
                $payment_data['total']
            );
            $payment_data['confirmed'] = $payment_data['total_confirmed_percent'] === $service::FULL_PERCENT_STACK;
        }

        $this->term->payment_data = $payment_data;

        return $this;
    }

    public function toArray(): ITermBuilder
    {
        $this->term = $this->term->toArray();

        return $this;
    }

    public function getTerm()
    {
        $term = $this->term;
        $this->create();

        return $term;
    }

    private function filterTerms(): void
    {
        if (!$this->term->triggers || !$this->term->order_id) {
            return;
        }

        if ($this->term->group_type === PaymentTermGroupTypes::SHIPMENTS) {
            $transport_hub_property = ShippingMethodsServiceProvider::getTransportHubProperty();

            $this->term->triggers = array_filter($this->term->triggers, function ($trigger) use ($transport_hub_property) {
                return $transport_hub_property->isSeparateByShipments($trigger['transport_hub_type']);
            });
        } else {
            $available_hub_types = ShippingMethodsServiceProvider::getTransportHubRepository()->getOrderTransportHubTypes($this->term->order_id, [
                'exclude_hub_types' => [TransportHubTypes::CUSTOM]
            ]);

            $this->term->triggers = array_filter($this->term->triggers, function ($trigger) use ($available_hub_types) {
                return in_array($trigger['transport_hub_type'], $available_hub_types);
            });
        }
    }
}
