<?php
/***************************************************************************
 *                                                                          *
 *   © Simtech Development Ltd.                                             *
 *                                                                          *
 * This  is  commercial  software,  only  users  who have purchased a valid *
 * license  and  accept  to the terms of the  License Agreement can install *
 * and use this program.                                                    *
 ***************************************************************************/

namespace Tygh\Addons\SdPaymentTerms\PaymentTerm\Checker;

use Tygh\Common\OperationResult;

interface IChecker
{
    public function setGroupList(array $group_list): IChecker;

    public function checkCreateOrderTerm(): IChecker;

    public function checkOrderOwner(): IChecker;

    public function checkIsApproved(bool $approved = true): IChecker;

    public function checkAllowApprove(): IChecker;

    public function checkAllowConfirmStepPayment(): IChecker;

    public function checkGroupType(): IChecker;

    public function checkGroupSteps(): IChecker;

    public function checkGroupProducts(): IChecker;

    public function checkStepTrigger(): IChecker;

    public function checkStepPercent(): IChecker;

    public function checkTotalPercent(): IChecker;

    public function getResult(): OperationResult;
}
