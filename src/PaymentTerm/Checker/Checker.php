<?php
/***************************************************************************
 *                                                                          *
 *   © Simtech Development Ltd.                                             *
 *                                                                          *
 * This  is  commercial  software,  only  users  who have purchased a valid *
 * license  and  accept  to the terms of the  License Agreement can install *
 * and use this program.                                                    *
 ***************************************************************************/

namespace Tygh\Addons\SdPaymentTerms\PaymentTerm\Checker;

use Tygh\Addons\SdPaymentTerms\PaymentTerm\Factory;
use Tygh\Addons\SdPaymentTerms\PaymentTerm\Repository;
use Tygh\Addons\SdPaymentTerms\Entity\Cache as EntityCache;
use Tygh\Addons\SdPaymentTerms\Models\PaymentTermTrigger;
use Tygh\Addons\SdPaymentTerms\PaymentTerm\StatusProperties;
use Tygh\Addons\SdPaymentTerms\Enum\PaymentTermGroupTypes;
use Tygh\Common\OperationResult;
use Tygh\Enum\ObjectStatuses;
use Tygh\Enum\SiteArea;
use Tygh\Enum\UserTypes;
use Tygh\Tygh;

class Checker implements IChecker
{
    const FULL_PERCENT_STACK = 100;

    protected $order_id;

    protected $factory;

    protected $repository;

    protected $group_list = [];

    private $operation_result;

    protected $order_info = [];

    public function __construct(int $order_id, Factory $factory, Repository $repository)
    {
        $this->order_id = $order_id;
        $this->factory = $factory;
        $this->repository = $repository;

        $this->createOperationResult();
        $this->order_info = EntityCache::getOrderInfo($this->order_id);
    }

    public function setGroupList(array $group_list): IChecker
    {
        $this->group_list = $group_list;

        return $this;
    }

    public function checkCreateOrderTerm(): IChecker
    {
        if ($this->skipCheck()) {
            return $this;
        }

        if (!empty($this->order_info['by_dealer_id'])) {
            $this->setOperationResultError(__('sd_payment_terms.checker.create_order_term_error'));
            return $this;
        }

        $errors = [];

        if (SiteArea::isStorefront(AREA)) {
            if (Tygh::$app['session']['auth']['user_type'] === UserTypes::CUSTOMER) {
                $errors[] = __('sd_payment_terms.checker.create_order_term_error');
            }
        } else {
            if (!$this->repository->isExistsByOrderId($this->order_info['order_id'] ?? 0)) {
                $errors[] = __('sd_payment_terms.checker.create_order_term_error');
            }
        }

        foreach ($errors as $message) {
            $this->setOperationResultError($message);
        }

        return $this;
    }

    public function checkOrderOwner(): IChecker
    {
        if ($this->skipCheck()) {
            return $this;
        }

        if (SiteArea::isStorefront(AREA)) {
            $allowed = fn_is_order_allowed($this->order_id, Tygh::$app['session']['auth']);
        } else {
            $runtime_company_id = fn_get_runtime_company_id();
            $order_company_id = $this->order_info['company_id'] ?? 0;

            if ($runtime_company_id) {
                $allowed = (int) $runtime_company_id === (int) $order_company_id;
            } else {
                $allowed = true;
            }
        }

        if (!$allowed) {
            $this->setOperationResultError(__('sd_payment_terms.checker.order_owner_error'));
        }

        return $this;
    }

    public function checkIsApproved(bool $approved = true): IChecker
    {
        if ($this->skipCheck()) {
            return $this;
        }

        $status = $this->order_info['payment_term_status'] ?? $this->repository->getOrderPaymentTermStatus($this->order_id);

        if ($approved !== StatusProperties::isApproved($status)) {
            $this->setOperationResultError(__('sd_payment_terms.checker.is_approved_error'));
        }

        return $this;
    }

    public function checkAllowApprove(): IChecker
    {
        if ($this->skipCheck()) {
            return $this;
        }

        $status = $this->order_info['payment_term_status'] ?? $this->repository->getOrderPaymentTermStatus($this->order_id);
        $allow_approve = StatusProperties::allowApprove($status) && $this->repository->isExistsByOrderId($this->order_id);

        if (!$allow_approve) {
            $this->setOperationResultError(__('sd_payment_terms.checker.allow_approve_error'));
        }

        return $this;
    }

    public function checkAllowConfirmStepPayment(): IChecker
    {
        if ($this->skipCheck()) {
            return $this;
        }

        $status = $this->order_info['payment_term_status'] ?? $this->repository->getOrderPaymentTermStatus($this->order_id);
        $allow_confirm = StatusProperties::AllowConfirmStepPayment($status) && $this->repository->isExistsByOrderId($this->order_id);

        if (!$allow_confirm) {
            $this->setOperationResultError(__('sd_payment_terms.checker.allow_confirm_step_payment_error'));
        }

        return $this;
    }

    public function checkGroupType(): IChecker
    {
        if ($this->skipCheck(true)) {
            return $this;
        }

        foreach ($this->group_list as $group_data) {
            $last_group_type = $last_group_type ?? $group_data['group_type'];

            if (
                !PaymentTermGroupTypes::isExist($group_data['group_type'])
                || $last_group_type !== $group_data['group_type']
            ) {
                $this->setOperationResultError(__('sd_payment_terms.checker.group_type_error'));
                break;
            }
        }

        return $this;
    }

    public function checkGroupSteps(): IChecker
    {
        if ($this->skipCheck(true)) {
            return $this;
        }

        foreach ($this->group_list as $group_data) {
            if (empty($group_data['steps'])) {
                $this->setOperationResultError(__('sd_payment_terms.checker.group_steps_error'));
                break;
            }
        }

        return $this;
    }

    public function checkGroupProducts(): IChecker
    {
        if ($this->skipCheck(true)) {
            return $this;
        }

        $group_data = $this->getFirstGroup();
        $group = $this->factory->createGroup($group_data['group_type'], $group_data);

        if (!$group->haveProducts()) {
            return $this;
        }

        $group_products = $this->getGroupProductsAmount();

        if (!$group_products) {
            $this->setOperationResultError(__('sd_payment_terms.checker.group_products_error'));
            return $this;
        }

        $products = $this->order_info['products'] ?? [];

        foreach ($products as $product) {
            if (!empty($product['extra']['exclude_from_calculate'])) {
                continue;
            }

            $has_error = true;

            foreach ($group_products as $item_id => $amount) {
                if (
                    (int) $product['item_id'] === (int) $item_id
                    && (int) $product['amount'] === (int) $amount
                ) {
                    $has_error = false;
                }
            }

            if ($has_error) {
                $this->setOperationResultError(__('sd_payment_terms.checker.group_products_error'));
                break;
            }
        }

        return $this;
    }

    public function checkStepTrigger(): IChecker
    {
        if ($this->skipCheck(true)) {
            return $this;
        }

        $trigger_ids = PaymentTermTrigger::model()->findAll([
            'get_ids' => true,
            'status'  => ObjectStatuses::ACTIVE
        ]);

        foreach ($this->getAllSteps() as $step_data) {
            if (!in_array($step_data['trigger_id'], $trigger_ids)) {
                $this->setOperationResultError(__('sd_payment_terms.checker.step_trigger_error'));
                break;
            }
        }

        return $this;
    }

    public function checkStepPercent(): IChecker
    {
        if ($this->skipCheck(true)) {
            return $this;
        }

        foreach ($this->getAllSteps() as $step_data) {
            if (empty($step_data['percent']) || $step_data['percent'] > self::FULL_PERCENT_STACK) {
                $this->setOperationResultError(__('sd_payment_terms.checker.step_percent_error'));
                break;
            }
        }

        return $this;
    }

    public function checkTotalPercent(): IChecker
    {
        if ($this->skipCheck(true)) {
            return $this;
        }

        $full_percent_stack = self::FULL_PERCENT_STACK;
        $total_percent = (int) array_sum(
            array_column($this->getAllSteps(), 'percent')
        );

        $group_data = $this->getFirstGroup();
        $group = $this->factory->createGroup($group_data['group_type'], $group_data);

        if ($group->haveProducts()) {
            $full_percent_stack *= count($this->group_list);
        }

        if ($total_percent !== $full_percent_stack) {
            $this->setOperationResultError(__('sd_payment_terms.checker.total_percent_error'));
        }

        return $this;
    }

    public function getResult(): OperationResult
    {
        $result = $this->operation_result;
        $this->createOperationResult();

        return $result;
    }

    private function skipCheck(bool $check_group_list = false): bool
    {
        return $check_group_list
            ? empty($this->group_list) || $this->operation_result->isFailure()
            : $this->operation_result->isFailure();
    }

    private function setOperationResultError(string $message = ''): void
    {
        $this->operation_result->setSuccess(false);

        if ($message) {
            $this->operation_result->setErrors([$message]);
        }
    }

    private function getFirstGroup(): array
    {
        return !empty($this->group_list) ? reset($this->group_list) : [];
    }

    protected function getGroupProductsAmount(): array
    {
        $group_products = [];

        foreach ($this->group_list as $group_data) {
            if (!empty($group_data['products'])) {
                foreach ($group_data['products'] as $group_product) {
                    $item_id = $group_product['item_id'];

                    if (isset($group_products[$item_id])) {
                        $group_products[$item_id] += $group_product['amount'];
                    } else {
                        $group_products[$item_id] = $group_product['amount'];
                    }
                }
            }
        }

        return $group_products;
    }

    protected function getAllSteps(): array
    {
        $steps = [];

        foreach ($this->group_list as $group_data) {
            $steps = array_merge($steps, $group_data['steps']);
        }

        return $steps;
    }

    private function createOperationResult(): void
    {
        $this->operation_result = new OperationResult(true);
    }
}
