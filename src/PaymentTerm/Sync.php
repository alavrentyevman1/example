<?php
/***************************************************************************
 *                                                                          *
 *   © Simtech Development Ltd.                                             *
 *                                                                          *
 * This  is  commercial  software,  only  users  who have purchased a valid *
 * license  and  accept  to the terms of the  License Agreement can install *
 * and use this program.                                                    *
 ***************************************************************************/

namespace Tygh\Addons\SdPaymentTerms\PaymentTerm;

use Tygh\Addons\SdPaymentTerms\ServiceProvider;

class Sync
{
    public function syncOrderPaymentTotalConfirmed(int $order_id): void
    {
        if (!$order_id) {
            return;
        }

        $group_type = ServiceProvider::getPaymentTermRepository()->getGroupTypeByOrderId($order_id);
        $payment_term_manager = ServiceProvider::getPaymentTermManager($order_id, $group_type);
        $payment_term = $payment_term_manager->getTermFromStorage();

        if (isset($payment_term['payment_data']['total_confirmed_percent'])) {
            db_query('UPDATE ?:orders SET payment_percent_confirmed = ?i WHERE order_id = ?i', $payment_term['payment_data']['total_confirmed_percent'], $order_id);
        }
    }
}
