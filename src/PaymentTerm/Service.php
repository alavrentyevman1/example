<?php
/***************************************************************************
 *                                                                          *
 *   © Simtech Development Ltd.                                             *
 *                                                                          *
 * This  is  commercial  software,  only  users  who have purchased a valid *
 * license  and  accept  to the terms of the  License Agreement can install *
 * and use this program.                                                    *
 ***************************************************************************/

namespace Tygh\Addons\SdPaymentTerms\PaymentTerm;

use Tygh\Addons\SdPaymentTerms\ServiceProvider;
use Tygh\Addons\SdPaymentTerms\Enum\PaymentTermGroupTypes;
use Tygh\Addons\SdShippingMethods\Batches\OrderIdMap as BatchesOrderIdMap;
use Tygh\Registry;
use Tygh\Tygh;
use Tygh\Enum\SiteArea;
use Tygh\Enum\UserTypes;
use DateTime;
use DateTimeZone;

class Service
{
    const FULL_PERCENT_STACK = 100;

    public function setGetTermFromSession(): void
    {
        Tygh::$app['session']['sd_payment_terms']['get_from_session'] = true;
    }

    public function needGetTermFromSession(): bool
    {
        return Tygh::$app['session']['sd_payment_terms']['get_from_session'] ?? false;
    }

    public function clearGetTermFromSession(): void
    {
        unset(Tygh::$app['session']['sd_payment_terms']['get_from_session']);
    }

    public function getBeginEndDateTimestamps(string $datetime): array
    {
        $date_now = new DateTime($datetime);

        $begin_of_day = clone $date_now;
        $begin_of_day->modify('today');

        $end_of_day = clone $date_now;
        $end_of_day->modify('tomorrow');

        return [$begin_of_day->getTimestamp(), $end_of_day->getTimestamp()];
    }

    public function formatDiffSecondsTime(int $diff_seconds, array $params, string $separator = ' : '): string
    {
        if ($diff_seconds <= 0) {
            return '';
        }

        $days    = floor($diff_seconds / SECONDS_IN_DAY);
        $hours   = floor(($diff_seconds - $days * SECONDS_IN_DAY) / SECONDS_IN_HOUR);
        $minutes = floor(($diff_seconds - $days * SECONDS_IN_DAY - $hours * SECONDS_IN_HOUR) / 60);
        $seconds = floor($diff_seconds - $days * SECONDS_IN_DAY - $hours * SECONDS_IN_HOUR - $minutes * 60);

        if (in_array('only_first', $params)) {
            $hours   = $days                       ? 0 : $hours;
            $minutes = $days || $hours             ? 0 : $minutes;
            $seconds = $days || $hours || $minutes ? 0 : $seconds;
        }

        if (in_array('increment_days', $params)) {
            $days = $days > 0 ? ++$days : $days;
        }

        $formatted_time = array_filter([
            'days'    => $days,
            'hours'   => $hours,
            'minutes' => $minutes,
            'seconds' => $seconds
        ]);

        foreach ($formatted_time as $type => &$time) {
            if (in_array($type, $params)) {
                $time = __('sd_payment_terms.n_' . $type, [$time]);
            } else {
                unset($formatted_time[$type]);
            }
        }

        return implode($separator, $formatted_time);
    }

    public function calculatePercentValue(float $current_value, float $from_value): int
    {
        if (!$from_value) {
            return 0;
        }

        $percent = (int) round($current_value / $from_value * self::FULL_PERCENT_STACK);

        if ($percent > self::FULL_PERCENT_STACK || $current_value === $from_value) {
            $percent = self::FULL_PERCENT_STACK;
        }

        return $percent;
    }

    public function getDefaultOrderPaymentTermData(int $order_id): array
    {
        $addon_settings = Registry::get('addons.sd_payment_terms');

        return [[
            'group_id'   => 1,
            'group_type' => PaymentTermGroupTypes::getDefault(),
            'order_id'   => $order_id,
            'steps'      => [[
                'step_id'      => 1,
                'group_type'   => PaymentTermGroupTypes::getDefault(),
                'group_id'     => 1,
                'order_id'     => $order_id,
                'trigger_id'   => $addon_settings['default_trigger_id'],
                'percent'      => 100,
                'payment_days' => $addon_settings['default_payment_days']
            ]]
        ]];
    }

    public function fillOrdersPaymentTermData(array $orders): array
    {
        $mapping_order_ids = [];

        foreach ($orders as $order) {
            if (empty($order['by_dealer_id']) && empty($order['payment_term_status'])) {
                $mapping_order_ids[$order['order_id']] = BatchesOrderIdMap::getFirstOrderId($order['order_id'], BatchesOrderIdMap::BY_DEALER_TYPE);
            }
        }

        if ($mapping_order_ids) {
            $payment_term_orders = db_get_hash_array(
                'SELECT order_id, payment_term_status, payment_percent_confirmed FROM ?:orders WHERE order_id IN (?n)',
                'order_id', $mapping_order_ids
            );
        }

        foreach ($orders as &$order) {
            $mapping_order_id = $mapping_order_ids[$order['order_id']] ?? 0;

            if (!empty($payment_term_orders[$mapping_order_id])) {
                $order['payment_term_status'] = $payment_term_orders[$mapping_order_id]['payment_term_status'];
                $order['payment_percent_confirmed'] = $payment_term_orders[$mapping_order_id]['payment_percent_confirmed'];
            }
        }

        return $orders;
    }

    public function getAreaMirrorReceivers(string $area = AREA): array
    {
        $receivers = [];

        if (SiteArea::isStorefront($area)) {
            $receivers[UserTypes::ADMIN] = $receivers[UserTypes::VENDOR] = true;
            $receivers[UserTypes::CUSTOMER] = false;
        } else {
            $receivers[UserTypes::ADMIN] = $receivers[UserTypes::VENDOR] = false;
            $receivers[UserTypes::CUSTOMER] = true;
        }

        return $receivers;
    }

    public function runCronNotifications(): void
    {
        $factory = ServiceProvider::getPaymentTermPaymentFactory();

        $payment_manager = $factory->create(null, 'AfterEndPaymentEvent');
        $payment_manager->sendEventNotifications();

        $payment_manager = $factory->create(null, 'TodayEndPaymentEvent');
        $payment_manager->sendEventNotifications();

        $payment_manager = $factory->create(null, 'BeforeEndPaymentEvent');
        $payment_manager->sendEventNotifications();
    }

    public function runCronTriggers(): void
    {
        $payment_terms_repository = ServiceProvider::getPaymentTermRepository();
        $factory = ServiceProvider::getPaymentTermPaymentFactory();
        $payment_manager = $factory->create('OrderTransportHubTrigger');

        $order_ids = $payment_terms_repository->getEmptyEndPaymentTimestampOrderIds();

        foreach ($order_ids as $order_id) {
            $payment_manager->setTriggerEndPaymentTimestamp($order_id, [
                'order_id' => $order_id
            ]);
        }
    }
}
