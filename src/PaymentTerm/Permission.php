<?php
/***************************************************************************
 *                                                                          *
 *   © Simtech Development Ltd.                                             *
 *                                                                          *
 * This  is  commercial  software,  only  users  who have purchased a valid *
 * license  and  accept  to the terms of the  License Agreement can install *
 * and use this program.                                                    *
 ***************************************************************************/

namespace Tygh\Addons\SdPaymentTerms\PaymentTerm;

use Tygh\Enum\SiteArea;
use Tygh\Tygh;

class Permission
{
    protected $repository;

    public function __construct(Repository $repository)
    {
        $this->repository = $repository;
    }

    public function canViewPaymentTerms(?int $user_id = null): bool
    {
        $user_id = $user_id === null ? Tygh::$app['session']['auth']['user_id'] : $user_id;

        return SiteArea::isStorefront(AREA) || fn_check_user_access($user_id, 'view_payment_terms');
    }

    public function canManagePaymentTerms(?int $order_id = null, ?int $user_id = null): bool
    {
        $user_id = $user_id === null ? Tygh::$app['session']['auth']['user_id'] : $user_id;
        $allow = SiteArea::isStorefront(AREA) || fn_check_user_access($user_id, 'manage_payment_terms');

        if ($allow && $order_id) {
            $status = $this->repository->getOrderPaymentTermStatus($order_id);
            $allow = StatusProperties::isNotApproved($status);
        }

        return $allow;
    }
}
