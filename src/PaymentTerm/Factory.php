<?php
/***************************************************************************
 *                                                                          *
 *   © Simtech Development Ltd.                                             *
 *                                                                          *
 * This  is  commercial  software,  only  users  who have purchased a valid *
 * license  and  accept  to the terms of the  License Agreement can install *
 * and use this program.                                                    *
 ***************************************************************************/

namespace Tygh\Addons\SdPaymentTerms\PaymentTerm;

use Tygh\Addons\SdPaymentTerms\PaymentTerm\Group\IGroup;
use Tygh\Addons\SdPaymentTerms\PaymentTerm\Step\IStep;
use Tygh\Exceptions\ClassNotFoundException;

class Factory
{
    protected $factory_schema;

    public function __construct(array $factory_schema)
    {
        $this->factory_schema = $factory_schema;
    }

    public function createGroup(string $type, array $group_data, array $extra_methods = []): IGroup
    {
        if (isset($this->factory_schema[$type]['group_class']) && class_exists($this->factory_schema[$type]['group_class'])) {
            $group = new $this->factory_schema[$type]['group_class']($group_data);
            $group = $this->runExtraMethods($group, $extra_methods);
        } else {
            throw new ClassNotFoundException("Undefined class for the payment term group type: {$type}");
        }

        return $group;
    }

    public function createStep(string $type, array $step_data, array $extra_methods = []): IStep
    {
        if (isset($this->factory_schema[$type]['step_class']) && class_exists($this->factory_schema[$type]['step_class'])) {
            $step = new $this->factory_schema[$type]['step_class']($step_data);
            $step = $this->runExtraMethods($step, $extra_methods);
        } else {
            throw new ClassNotFoundException("Undefined class for the payment term step type: {$type}");
        }

        return $step;
    }

    private function runExtraMethods($instance, array $extra_methods)
    {
        foreach ($extra_methods as $extra_method) {
            if (method_exists($instance, $extra_method)) {
                $instance->$extra_method();
            }
        }

        return $instance;
    }
}
