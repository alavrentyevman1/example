<?php
/***************************************************************************
 *                                                                          *
 *   © Simtech Development Ltd.                                             *
 *                                                                          *
 * This  is  commercial  software,  only  users  who have purchased a valid *
 * license  and  accept  to the terms of the  License Agreement can install *
 * and use this program.                                                    *
 ***************************************************************************/

namespace Tygh\Addons\SdPaymentTerms\PaymentTerm\Payment;

use Tygh\Addons\SdPaymentTerms\PaymentTerm\Payment\Trigger\ITrigger;
use Tygh\Addons\SdPaymentTerms\PaymentTerm\Payment\Event\IEvent;

class Manager
{
    protected $trigger;

    protected $event;

    public function __construct(?ITrigger $trigger, ?IEvent $event)
    {
        $this->trigger = $trigger;
        $this->event = $event;
    }

    public function setTriggerEndPaymentTimestamp(int $order_id, array $params): void
    {
        $this->trigger->setEndPaymentTimestamp($order_id, $params);
    }

    public function sendEventNotifications(): void
    {
        $this->event->sendNotifications();
    }
}
