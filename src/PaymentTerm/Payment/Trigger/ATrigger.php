<?php
/***************************************************************************
 *                                                                          *
 *   © Simtech Development Ltd.                                             *
 *                                                                          *
 * This  is  commercial  software,  only  users  who have purchased a valid *
 * license  and  accept  to the terms of the  License Agreement can install *
 * and use this program.                                                    *
 ***************************************************************************/

namespace Tygh\Addons\SdPaymentTerms\PaymentTerm\Payment\Trigger;

use Tygh\Addons\SdPaymentTerms\ServiceProvider;
use Tygh\Addons\SdPaymentTerms\Enum\PaymentTermStatuses;

abstract class ATrigger implements ITrigger
{
    public function setEndPaymentTimestamp(int $order_id, array $params): void
    {
        $payment_term_repository = ServiceProvider::getPaymentTermRepository();
        $order_payment_term_status = $payment_term_repository->getOrderPaymentTermStatus($order_id);

        if (
            !$order_id
            || $order_payment_term_status !== PaymentTermStatuses::FULL_APPROVE
        ) {
            return;
        }

        $trigger_ids = $this->getTriggerIds($params);

        if (!$trigger_ids) {
            return;
        }

        $steps = $payment_term_repository->findStepsFromStorage($order_id, [
            'trigger_ids' => $trigger_ids
        ]);

        array_walk($steps, function ($step) {
            $step_data = $step->toArray();

            if (empty($step_data['end_payment_timestamp'])) {
                $step->saveToStorage([
                    'end_payment_timestamp' => TIME + $step_data['payment_days'] * SECONDS_IN_DAY
                ]);
            }
        });
    }

    abstract protected function getTriggerIds(array $params): array;
}
