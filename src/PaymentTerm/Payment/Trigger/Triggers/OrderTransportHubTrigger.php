<?php
/***************************************************************************
 *                                                                          *
 *   © Simtech Development Ltd.                                             *
 *                                                                          *
 * This  is  commercial  software,  only  users  who have purchased a valid *
 * license  and  accept  to the terms of the  License Agreement can install *
 * and use this program.                                                    *
 ***************************************************************************/

namespace Tygh\Addons\SdPaymentTerms\PaymentTerm\Payment\Trigger\Triggers;

use Tygh\Addons\SdPaymentTerms\Models\PaymentTermTrigger;
use Tygh\Addons\SdPaymentTerms\PaymentTerm\Payment\Trigger\ATrigger;
use Tygh\Enum\ObjectStatuses;

class OrderTransportHubTrigger extends ATrigger
{
    protected function getTriggerIds(array $params): array
    {
        return PaymentTermTrigger::model()->findAll([
            'status'  => ObjectStatuses::ACTIVE,
            'get_ids' => true
        ]);
    }
}
