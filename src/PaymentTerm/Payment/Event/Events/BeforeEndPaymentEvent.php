<?php
/***************************************************************************
 *                                                                          *
 *   © Simtech Development Ltd.                                             *
 *                                                                          *
 * This  is  commercial  software,  only  users  who have purchased a valid *
 * license  and  accept  to the terms of the  License Agreement can install *
 * and use this program.                                                    *
 ***************************************************************************/

namespace Tygh\Addons\SdPaymentTerms\PaymentTerm\Payment\Event\Events;

use Tygh\Addons\SdPaymentTerms\PaymentTerm\Payment\Event\AEvent;
use Tygh\Addons\SdPaymentTerms\Enum\PaymentTermNotificationTypes;
use Tygh\Registry;
use Tygh\Enum\YesNo;

class BeforeEndPaymentEvent extends AEvent
{
    public function getNotificationType(): string
    {
        return PaymentTermNotificationTypes::BEFORE_END_PAYMENT;
    }

    public function getNotificationEventId(): string
    {
        return 'payment_terms.before_end_payment_event';
    }

    public function getQueryConditions(): array
    {
        $conditions = parent::getQueryConditions();

        $settings = Registry::get('addons.sd_payment_terms');

        if (empty($settings['before_end_payment_notification_event_days'])) {
            // Need for skip get steps
            $conditions['end_payment_timestamp'] = -1;

            return $conditions;
        }

        if ($settings['before_end_payment_notification_event_everyday'] === YesNo::YES) {
            // Clear last_notification_type for send event notification again
            db_query(
                'UPDATE ?:order_payment_term_steps SET last_notification_type = ?s WHERE ?w',
                PaymentTermNotificationTypes::EMPTY,
                [
                    ['last_notification_type', 'IN', [PaymentTermNotificationTypes::BEFORE_END_PAYMENT]],
                    ['last_notification_timestamp', '>=', TIME + SECONDS_IN_DAY]
                ]
            );
        }

        $conditions[] = ['last_notification_type', 'IN', [
            PaymentTermNotificationTypes::EMPTY
        ]];

        $before_end_payment_timestamp = TIME + $settings['before_end_payment_notification_event_days'] * SECONDS_IN_DAY;
        $conditions[] = ['end_payment_timestamp', '>=', TIME];
        $conditions[] = ['end_payment_timestamp', '<=', $before_end_payment_timestamp];

        return $conditions;
    }
}
