<?php
/***************************************************************************
 *                                                                          *
 *   © Simtech Development Ltd.                                             *
 *                                                                          *
 * This  is  commercial  software,  only  users  who have purchased a valid *
 * license  and  accept  to the terms of the  License Agreement can install *
 * and use this program.                                                    *
 ***************************************************************************/

namespace Tygh\Addons\SdPaymentTerms\PaymentTerm\Payment\Event\Events;

use Tygh\Addons\SdPaymentTerms\PaymentTerm\Payment\Event\AEvent;
use Tygh\Addons\SdPaymentTerms\Enum\PaymentTermNotificationTypes;

class AfterEndPaymentEvent extends AEvent
{
    public function getNotificationType(): string
    {
        return PaymentTermNotificationTypes::AFTER_END_PAYMENT;
    }

    public function getNotificationEventId(): string
    {
        return 'payment_terms.after_end_payment_event';
    }

    public function getQueryConditions(): array
    {
        $conditions = parent::getQueryConditions();

        $conditions[] = ['last_notification_type', 'IN', [
            PaymentTermNotificationTypes::EMPTY,
            PaymentTermNotificationTypes::BEFORE_END_PAYMENT,
            PaymentTermNotificationTypes::TODAY_END_PAYMENT
        ]];

        $conditions[] = ['end_payment_timestamp', '<=', TIME];

        return $conditions;
    }
}
