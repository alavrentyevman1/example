<?php
/***************************************************************************
 *                                                                          *
 *   © Simtech Development Ltd.                                             *
 *                                                                          *
 * This  is  commercial  software,  only  users  who have purchased a valid *
 * license  and  accept  to the terms of the  License Agreement can install *
 * and use this program.                                                    *
 ***************************************************************************/

namespace Tygh\Addons\SdPaymentTerms\PaymentTerm\Payment\Event;

use Tygh\Addons\SdPaymentTerms\ServiceProvider;
use Tygh\Tygh;

abstract class AEvent implements IEvent
{
    protected $notification_type;

    protected $notification_event_id;

    protected $service;

    public function __construct()
    {
        $this->notification_type = $this->getNotificationType();
        $this->notification_event_id = $this->getNotificationEventId();
        $this->service = ServiceProvider::getPaymentTermService();
    }

    abstract function getNotificationType(): string;

    abstract function getNotificationEventId(): string;

    public function getSteps(): array
    {
        return db_get_array(
            'SELECT ?p FROM ?:order_payment_term_steps ?p WHERE ?w',
            implode(', ', $this->getQueryFields()),
            implode(' ', $this->getQueryJoins()),
            $this->getQueryConditions()
        );
    }

    public function sendNotifications(): void
    {
        /** @var \Tygh\Notifications\EventDispatcher $event_dispatcher */
        $event_dispatcher = Tygh::$app['event.dispatcher'];
        $factory = ServiceProvider::getPaymentTermFactory();

        array_map(function ($step_data) use ($event_dispatcher, $factory) {
            $step = $factory->createStep($step_data['group_type'], $step_data, ['setAbsoluteAmount']);
            $step_data['absolute_amount'] = $step->toArray()['absolute_amount'];

            $event_dispatcher->dispatch($this->notification_event_id, [
                'step_data' => $step_data
            ]);

            $step->saveToStorage([
                'last_notification_type'      => $this->notification_type,
                'last_notification_timestamp' => TIME
            ]);
        }, $this->getSteps());
    }

    protected function getQueryFields(): array
    {
        return [
            '?:order_payment_term_steps.*',
            '?:orders.email',
            '?:orders.company_id',
            '?:orders.lang_code'
        ];
    }

    protected function getQueryJoins(): array
    {
        return [
            db_quote(' INNER JOIN ?:orders ON ?:orders.order_id = ?:order_payment_term_steps.order_id')
        ];
    }

    protected function getQueryConditions(): array
    {
        return [
            ['end_payment_timestamp', '<>', 0],
            ['payment_timestamp', '=', 0]
        ];
    }
}
