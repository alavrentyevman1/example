<?php
/***************************************************************************
 *                                                                          *
 *   © Simtech Development Ltd.                                             *
 *                                                                          *
 * This  is  commercial  software,  only  users  who have purchased a valid *
 * license  and  accept  to the terms of the  License Agreement can install *
 * and use this program.                                                    *
 ***************************************************************************/

namespace Tygh\Addons\SdPaymentTerms\PaymentTerm\Payment;

use Tygh\Exceptions\ClassNotFoundException;

class Factory
{
    public function create(?string $trigger_class = null, ?string $event_class = null): Manager
    {
        $trigger = $event = null;

        if ($trigger_class) {
            $trigger = $this->createInstance(
                '\\Tygh\\Addons\\SdPaymentTerms\\PaymentTerm\\Payment\\Trigger\\Triggers\\' . ucfirst($trigger_class)
            );
        }

        if ($event_class) {
            $event = $this->createInstance(
                '\\Tygh\\Addons\\SdPaymentTerms\\PaymentTerm\\Payment\\Event\\Events\\' . ucfirst($event_class)
            );
        }

        return new Manager($trigger, $event);
    }

    private function createInstance(string $class)
    {
        if (class_exists($class)) {
            $instance = new $class;
        } else {
            throw new ClassNotFoundException("Undefined class: {$class}");
        }

        return $instance;
    }
}
