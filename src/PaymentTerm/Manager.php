<?php
/***************************************************************************
 *                                                                          *
 *   © Simtech Development Ltd.                                             *
 *                                                                          *
 * This  is  commercial  software,  only  users  who have purchased a valid *
 * license  and  accept  to the terms of the  License Agreement can install *
 * and use this program.                                                    *
 ***************************************************************************/

namespace Tygh\Addons\SdPaymentTerms\PaymentTerm;

use Tygh\Addons\SdPaymentTerms\PaymentTerm\Term\ITermBuilder;
use Tygh\Addons\SdPaymentTerms\PaymentTerm\Checker\Checker;
use Tygh\Addons\SdPaymentTerms\Enum\PaymentTermGroupTypes;
use Tygh\Common\OperationResult;
use Tygh\Registry;

class Manager
{
    protected $order_id;

    protected $type;

    protected $checker;

    protected $term_builder;

    protected $factory;

    protected $repository;

    private $session = [];

    public function __construct(int $order_id, string $type, Checker $checker, ITermBuilder $term_builder, Factory $factory, Repository $repository)
    {
        $this->order_id = $order_id;
        $this->type = $type;
        $this->checker = $checker;
        $this->term_builder = $term_builder;
        $this->factory = $factory;
        $this->repository = $repository;

        $this->type = $this->type ?: PaymentTermGroupTypes::getDefault();
        $this->session = &$this->repository->getFromSession($this->order_id);
    }

    public function changeTermGroupTypeFromSession(): void
    {
        $this->clearTermFromSession();
    }

    public function saveTermGroupToSession(int $group_id, array $data): void
    {
        $data['group_data']['group_id'] = $group_id;

        $group = $this->factory->createGroup($this->type, $data['group_data'])->setGroupIdBySession($this->session);
        $this->session = $group->saveToSession($this->session);
    }

    public function deleteTermGroupFromSession(int $group_id): void
    {
        $group = $this->factory->createGroup($this->type, [
            'group_id' => $group_id
        ]);

        $this->session = $group->deleteFromSession($this->session);
    }

    public function saveTermStepToSession(int $group_id, int $step_id, array $data): void
    {
        $data['step_data']['step_id'] = $step_id;
        $group_data = $this->repository->getGroupFromSession($this->order_id, $group_id);

        if (!$group_data) {
            $data['group_data']['group_id'] = $group_id;
            $group = $this->factory->createGroup($this->type, $data['group_data'])->setGroupIdBySession($this->session);
            $this->session = $group->saveToSession($this->session);
            $data['step_data']['group_id'] = $group->getGroupId();
        }

        $step = $this->factory->createStep($this->type, $data['step_data'])->setStepIdBySession($this->session);
        $this->session = $step->saveToSession($this->session);
    }

    public function deleteTermStepFromSession(int $group_id, int $step_id): void
    {
        $step = $this->factory->createStep($this->type, [
            'step_id'  => $step_id,
            'group_id' => $group_id
        ]);

        $this->session = $step->deleteFromSession($this->session);

        if (!$step->hasEmptyGroup()) {
            $group_data = $this->repository->getGroupFromSession($this->order_id, $group_id);
            if (empty($group_data['steps'])) {
                $this->deleteTermGroupFromSession($group_id);
            }
        }
    }

    public function fillTermGroupStepsToSession(array $group_list): void
    {
        foreach ($group_list as $group_data) {
            $this->saveTermGroupToSession($group_data['group_id'], [
                'group_data' => $group_data
            ]);

            foreach ($group_data['steps'] as $step_data) {
                $this->saveTermStepToSession($step_data['group_id'], $step_data['step_id'], [
                    'group_data' => $group_data,
                    'step_data'  => $step_data
                ]);
            }
        }
    }

    public function allowCreateTerm(): OperationResult
    {
        return $this->checker
            ->checkCreateOrderTerm()
            ->getResult();
    }

    public function allowUpdateTerm(): OperationResult
    {
       return $this->checker
            ->checkCreateOrderTerm()
            ->checkOrderOwner()
            ->getResult();
    }

    public function allowSaveTerm(array $group_list): OperationResult
    {
        $payment_term = $this->term_builder
            ->setGroupsWithSteps($group_list)
            ->toArray()
            ->getTerm();

        return $this->checker
            ->setGroupList($payment_term['groups'])
            ->checkCreateOrderTerm()
            ->checkOrderOwner()
            ->checkIsApproved(false)
            ->checkGroupType()
            ->checkGroupSteps()
            ->checkGroupProducts()
            ->checkStepTrigger()
            ->checkStepPercent()
            ->checkTotalPercent()
            ->getResult();
    }

    public function allowApproveTerm(): OperationResult
    {
        return $this->checker
            ->checkCreateOrderTerm()
            ->checkOrderOwner()
            ->checkAllowApprove()
            ->getResult();
    }

    public function allowConfirmStepPayment(): OperationResult
    {
        return $this->checker
            ->checkCreateOrderTerm()
            ->checkOrderOwner()
            ->checkAllowConfirmStepPayment()
            ->getResult();
    }

    public function saveTerm(array $group_list): bool
    {
        if (!$this->order_id) {
            return false;
        }

        $updated = [];

        $deleted = $this->deleteTermFromStorage();

        $payment_term = $this->term_builder
            ->setGroupsWithSteps($group_list)
            ->getTerm();

        foreach ($payment_term->groups as $group) {
            $updated[] = $group->saveToStorage();

            foreach ($group->getSteps() as $step) {
                $updated[] = $step->saveToStorage();
            }
        }

        if ($deleted && !$payment_term->groups) {
            $updated[] = true;
        }

        return !empty(array_filter($updated));
    }

    public function approveTerm(): bool
    {
        if (!$this->order_id) {
            return false;
        }

        return $this->term_builder
            ->setOrderId($this->order_id)
            ->getTerm()
            ->approve();
    }

    public function clearApproveTerm(): bool
    {
        if (!$this->order_id) {
            return false;
        }

        return $this->term_builder
            ->setOrderId($this->order_id)
            ->getTerm()
            ->clearApprove();
    }

    public function confirmStepPayment(int $group_id, int $step_id): bool
    {
        $confirmed = false;

        if (!$this->order_id || !$group_id || !$step_id) {
            return $confirmed;
        }

        $steps = $this->repository->findStepsFromStorage($this->order_id, [
            'group_id' => $group_id,
            'step_ids' => (array) $step_id
        ]);

        if ($steps) {
            $confirmed = reset($steps)->saveToStorage([
                'payment_timestamp' => TIME
            ]);
        }

        return $confirmed;
    }

    public function getTermFromSession(array $data = []): array
    {
        return $this->term_builder
            ->setOrderId($this->order_id)
            ->setGroupsWithSteps($this->repository->getFromSession($this->order_id))
            ->withGroupProperties()
            ->withGroupType($data['group_type'] ?? '')
            ->withTriggers()
            ->withStatusProperties()
            ->withPaymentData()
            ->toArray()
            ->getTerm();
    }

    public function getTermFromStorage(): array
    {
        return $this->term_builder
            ->setOrderId($this->order_id)
            ->setGroupsWithSteps($this->repository->getFromStorage($this->order_id))
            ->withGroupProperties()
            ->withGroupType()
            ->withTriggers()
            ->withStatusProperties()
            ->withPaymentData()
            ->toArray()
            ->getTerm();
    }

    public function clearTermFromSession(): void
    {
        $this->session = [];
    }

    public function deleteTermFromStorage(): bool
    {
        $deleted = [];
        $payment_term = $this->term_builder
            ->setGroupsWithSteps($this->repository->getFromStorage($this->order_id))
            ->getTerm();

        foreach ($payment_term->groups as $group) {
            $deleted[] = $group->deleteFromStorage();

            foreach ($group->getSteps() as $step) {
                $deleted[] = $step->deleteFromStorage();
            }
        }

        return !empty(array_filter($deleted));
    }

    public function cloneTermToOrder(int $order_id): bool
    {
        if ($this->order_id === $order_id) {
            return false;
        }

        $cloned = [];

        $payment_term_list = $this->repository->getFromStorage($this->order_id);

        $payment_term = $this->term_builder
            ->setGroupsWithSteps($payment_term_list)
            ->getTerm();

        foreach ($payment_term->groups as $group) {
            $cloned[] = $group->saveToStorage();

            foreach ($group->getSteps() as $step) {
                $cloned[] = $step->saveToStorage();
            }
        }

        return !empty(array_filter($cloned));
    }
}
