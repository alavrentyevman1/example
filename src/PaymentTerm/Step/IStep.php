<?php
/***************************************************************************
 *                                                                          *
 *   © Simtech Development Ltd.                                             *
 *                                                                          *
 * This  is  commercial  software,  only  users  who have purchased a valid *
 * license  and  accept  to the terms of the  License Agreement can install *
 * and use this program.                                                    *
 ***************************************************************************/

namespace Tygh\Addons\SdPaymentTerms\PaymentTerm\Step;

interface IStep
{
    const SESSION_PAYMENT_TERM_GROUP_STEPS_FIELD = 'steps';

    public function getGroupType(): string;

    public function setStepIdBySession(array $session): IStep;

    public function getStepId(): int;

    public function hasEmptyGroup(): bool;

    public function toArray(): array;

    public function saveToSession(array $session): array;

    public function deleteFromSession(array $session): array;

    public function saveToStorage(array $step_data = []): bool;

    public function deleteFromStorage(): bool;

    public function setTriggerName(): void;

    public function setAbsoluteAmount(): void;

    public function setPaymentData(): void;
}
