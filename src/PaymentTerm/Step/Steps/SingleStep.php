<?php
/***************************************************************************
 *                                                                          *
 *   © Simtech Development Ltd.                                             *
 *                                                                          *
 * This  is  commercial  software,  only  users  who have purchased a valid *
 * license  and  accept  to the terms of the  License Agreement can install *
 * and use this program.                                                    *
 ***************************************************************************/

namespace Tygh\Addons\SdPaymentTerms\PaymentTerm\Step\Steps;

use Tygh\Addons\SdPaymentTerms\PaymentTerm\Step\AStep;
use Tygh\Addons\SdPaymentTerms\Enum\PaymentTermGroupTypes;

class SingleStep extends AStep
{
    public function getGroupType(): string
    {
        return PaymentTermGroupTypes::SINGLE;
    }

    public function hasEmptyGroup(): bool
    {
        return false;
    }
}
