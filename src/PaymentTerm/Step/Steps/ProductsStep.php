<?php
/***************************************************************************
 *                                                                          *
 *   © Simtech Development Ltd.                                             *
 *                                                                          *
 * This  is  commercial  software,  only  users  who have purchased a valid *
 * license  and  accept  to the terms of the  License Agreement can install *
 * and use this program.                                                    *
 ***************************************************************************/

namespace Tygh\Addons\SdPaymentTerms\PaymentTerm\Step\Steps;

use Tygh\Addons\SdPaymentTerms\ServiceProvider;
use Tygh\Addons\SdPaymentTerms\PaymentTerm\Step\AStep;
use Tygh\Addons\SdPaymentTerms\Enum\PaymentTermGroupTypes;

class ProductsStep extends AStep
{
    public function getGroupType(): string
    {
        return PaymentTermGroupTypes::PRODUCTS;
    }

    public function hasEmptyGroup(): bool
    {
        return true;
    }

    public function setAbsoluteAmount(): void
    {
        $repository = ServiceProvider::getPaymentTermRepository();
        $factory = ServiceProvider::getPaymentTermFactory();

        $group_data = $repository->getGroupFromSession($this->order_id, $this->group_id);

        if (!$group_data) {
            return;
        }

        $absolute_amount = 0;

        $group_data = $factory->createGroup($group_data['group_type'], $group_data, [
            'setGroupTotals'
        ])->toArray();

        if (!empty($group_data['group_totals']['total'])) {
            $absolute_amount = fn_format_price($group_data['group_totals']['total'] / 100 * $this->percent);
        }

        $this->absolute_amount = (float) $absolute_amount;
    }
}
