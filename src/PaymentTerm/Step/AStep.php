<?php
/***************************************************************************
 *                                                                          *
 *   © Simtech Development Ltd.                                             *
 *                                                                          *
 * This  is  commercial  software,  only  users  who have purchased a valid *
 * license  and  accept  to the terms of the  License Agreement can install *
 * and use this program.                                                    *
 ***************************************************************************/

namespace Tygh\Addons\SdPaymentTerms\PaymentTerm\Step;

use Tygh\Addons\SdPaymentTerms\ServiceProvider;
use Tygh\Addons\SdPaymentTerms\Entity\Cache as EntityCache;
use Tygh\Addons\SdPaymentTerms\Entity\CommonTrait as EntityCommonTrait;
use Tygh\Addons\SdPaymentTerms\Models\PaymentTermTrigger;
use Tygh\Enum\ObjectStatuses;

abstract class AStep implements IStep
{
    use EntityCommonTrait;

    protected $step_id;

    protected $group_type;

    protected $group_id;

    protected $order_id;

    protected $trigger_id;

    protected $trigger_name;

    protected $percent;

    protected $payment_days;

    protected $end_payment_timestamp;

    protected $payment_timestamp;

    protected $last_notification_type;

    protected $last_notification_timestamp;

    protected $absolute_amount;

    protected $payment_data = [];

    protected $fillable = [
        'order_id',
        'step_id',
        'group_id',
        'group_type',
        'trigger_id',
        'percent',
        'payment_days'
    ];

    public function __construct(array $step_data)
    {
        $this->step_id                     = isset($step_data['step_id'])                     ? (int)    $step_data['step_id']                     : 0;
        $this->group_id                    = isset($step_data['group_id'])                    ? (int)    $step_data['group_id']                    : 0;
        $this->order_id                    = isset($step_data['order_id'])                    ? (int)    $step_data['order_id']                    : 0;
        $this->trigger_id                  = isset($step_data['trigger_id'])                  ? (int)    $step_data['trigger_id']                  : 0;
        $this->trigger_name                = isset($step_data['trigger_name'])                ? (string) $step_data['trigger_name']                : '';
        $this->percent                     = isset($step_data['percent'])                     ? (int)    $step_data['percent']                     : 0;
        $this->payment_days                = isset($step_data['payment_days'])                ? (int)    $step_data['payment_days']                : 0;
        $this->end_payment_timestamp       = isset($step_data['end_payment_timestamp'])       ? (int)    $step_data['end_payment_timestamp']       : 0;
        $this->payment_timestamp           = isset($step_data['payment_timestamp'])           ? (int)    $step_data['payment_timestamp']           : 0;
        $this->last_notification_type      = isset($step_data['last_notification_type'])      ? (string) $step_data['last_notification_type']      : '';
        $this->last_notification_timestamp = isset($step_data['last_notification_timestamp']) ? (int)    $step_data['last_notification_timestamp'] : 0;

        $this->group_type = $this->getGroupType();
    }

    public function setStepIdBySession(array $session): IStep
    {
        if (!$this->step_id && $this->group_id) {
            $step_ids = array_column($session[$this->group_id][self::SESSION_PAYMENT_TERM_GROUP_STEPS_FIELD] ?? [], 'step_id');
            $this->step_id = max($step_ids) + 1;
        }

        return $this;
    }

    public function getStepId(): int
    {
        return (int) $this->step_id;
    }

    public function toArray(): array
    {
        return [
            'step_id'                     => $this->step_id,
            'group_type'                  => $this->group_type,
            'group_id'                    => $this->group_id,
            'order_id'                    => $this->order_id,
            'trigger_id'                  => $this->trigger_id,
            'trigger_name'                => $this->trigger_name,
            'percent'                     => $this->percent,
            'payment_days'                => $this->payment_days,
            'end_payment_timestamp'       => $this->end_payment_timestamp,
            'payment_timestamp'           => $this->payment_timestamp,
            'last_notification_type'      => $this->last_notification_type,
            'last_notification_timestamp' => $this->last_notification_timestamp,
            'absolute_amount'             => $this->absolute_amount,
            'payment_data'                => $this->payment_data
        ];
    }

    public function saveToSession(array $session): array
    {
        if (!$this->step_id || !$this->group_id || empty($session[$this->group_id])) {
            return $session;
        }

        if (empty($session[$this->group_id][self::SESSION_PAYMENT_TERM_GROUP_STEPS_FIELD])) {
            $session[$this->group_id][self::SESSION_PAYMENT_TERM_GROUP_STEPS_FIELD] = [];
        }

        $session[$this->group_id][self::SESSION_PAYMENT_TERM_GROUP_STEPS_FIELD][$this->step_id] = $this->filterFillableFields(
            $this->toArray()
        );

        return $session;
    }

    public function deleteFromSession(array $session): array
    {
        unset($session[$this->group_id][self::SESSION_PAYMENT_TERM_GROUP_STEPS_FIELD][$this->step_id]);

        return $session;
    }

    public function saveToStorage(array $step_data = []): bool
    {
        return (bool) db_replace_into(
            'order_payment_term_steps',
            array_merge($step_data, $this->filterFillableFields(
                $this->toArray()
            ))
        );
    }

    public function deleteFromStorage(): bool
    {
        return (bool) db_query('DELETE FROM ?:order_payment_term_steps WHERE ?w', [
            'step_id'  => $this->step_id,
            'order_id' => $this->order_id,
            'group_id' => $this->group_id
        ]);
    }

    public function setTriggerName(): void
    {
        static $trigger_names;

        if ($trigger_names === null) {
            $triggers = PaymentTermTrigger::model()->findAll([
                'to_array' => true,
                'status'   => ObjectStatuses::ACTIVE
            ]);

            $trigger_names = array_combine(array_column($triggers, 'trigger_id'), array_column($triggers, 'name'));
        }

        $this->trigger_name = $trigger_names[$this->trigger_id] ?? '{{' . __('na') . '}}';;
    }

    public function setAbsoluteAmount(): void
    {
        $absolute_amount = 0;
        $order_info = EntityCache::getOrderInfo($this->order_id);

        if (!empty($order_info['total'])) {
            $absolute_amount = fn_format_price($order_info['total'] / 100 * $this->percent);
        }

        $this->absolute_amount = (float) $absolute_amount;
    }

    public function setPaymentData(): void
    {
        $service = ServiceProvider::getPaymentTermService();

        $payment_data = [
            'started'                    => !empty($this->end_payment_timestamp),
            'confirmed'                  => !empty($this->payment_timestamp),
            'overdue'                    => false,
            'in_progress_seconds'        => 0,
            'before_end_seconds'         => 0,
            'after_end_seconds'          => 0,
            'in_progress_time_formatted' => '',
            'before_end_time_formatted'  => '',
            'after_end_time_formatted'   => '',
            'progress_percent'           => $service::FULL_PERCENT_STACK,
        ];

        if (empty($payment_data['started'])) {
            $this->payment_data = $payment_data;
            return;
        }

        $format_time_params = ['only_first', 'increment_days', 'days', 'hours', 'minutes', 'seconds'];

        $payment_data['in_progress_seconds'] = TIME - ($this->end_payment_timestamp - $this->payment_days * SECONDS_IN_DAY);
        $payment_data['in_progress_time_formatted'] = $service->formatDiffSecondsTime($payment_data['in_progress_seconds'], $format_time_params);

        if (empty($payment_data['confirmed'])) {
            $payment_data['before_end_seconds'] = $this->end_payment_timestamp - TIME;
            $payment_data['before_end_time_formatted'] = $service->formatDiffSecondsTime($payment_data['before_end_seconds'], $format_time_params);

            if ($payment_data['before_end_seconds'] > 0) {
                $payment_data['progress_percent'] = $service->calculatePercentValue(
                    $payment_data['in_progress_seconds'],
                    $payment_data['before_end_seconds'] + $payment_data['in_progress_seconds']
                );
            } else {
                $payment_data['overdue'] = true;
                $payment_data['after_end_seconds'] = abs($this->end_payment_timestamp - TIME);
                $payment_data['after_end_time_formatted'] = $service->formatDiffSecondsTime($payment_data['after_end_seconds'], $format_time_params);
            }
        }

        $this->payment_data = $payment_data;
    }
}
