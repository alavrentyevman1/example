<?php
/***************************************************************************
 *                                                                          *
 *   © Simtech Development Ltd.                                             *
 *                                                                          *
 * This  is  commercial  software,  only  users  who have purchased a valid *
 * license  and  accept  to the terms of the  License Agreement can install *
 * and use this program.                                                    *
 ***************************************************************************/

namespace Tygh\Addons\SdPaymentTerms\PaymentTerm;

use Tygh\Addons\SdPaymentTerms\Enum\PaymentTermStatuses;
use Tygh\Enum\SiteArea;

class StatusProperties
{
    public static $schema = null;

    public static function getApproveStatusBySiteArea(string $area = AREA): string
    {
        return SiteArea::isStorefront($area) ? PaymentTermStatuses::CUSTOMER_APPROVE : PaymentTermStatuses::ADMIN_APPROVE;
    }

    public static function isApproved(string $status): bool
    {
        $status_properties = self::getStatusProperties($status);

        return !empty($status_properties['is_approved']);
    }

    public static function isNotApproved(string $status): bool
    {
        return !self::isApproved($status);
    }

    public static function allowApprove(string $status, string $area = AREA): bool
    {
        $status_properties = self::getStatusProperties($status);

        return !empty($status_properties['allow_approve'][$area]);
    }

    public static function isFullApprove(string $status, string $area = AREA): bool
    {
        $status_properties = self::getStatusProperties($status);

        return !empty($status_properties['is_full_approve'][$area]);
    }

    public static function allowConfirmStepPayment(string $status): bool
    {
        $status_properties = self::getStatusProperties($status);

        return SiteArea::isAdmin(AREA) && !empty($status_properties['allow_confirm_step_payment']);
    }

    public static function getStatusProperties(string $status): array
    {
        if (self::$schema === null) {
            self::$schema = fn_get_schema('sd_payment_terms', 'payment_term_statuses');
        }

        return self::$schema[$status] ?? [];
    }
}
