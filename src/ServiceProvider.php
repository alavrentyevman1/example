<?php
/***************************************************************************
 *                                                                          *
 *   © Simtech Development Ltd.                                             *
 *                                                                          *
 * This  is  commercial  software,  only  users  who have purchased a valid *
 * license  and  accept  to the terms of the  License Agreement can install *
 * and use this program.                                                    *
 ***************************************************************************/

namespace Tygh\Addons\SdPaymentTerms;

use Pimple\Container;
use Pimple\ServiceProviderInterface;
use Tygh\Addons\SdPaymentTerms\HookHandlers\OrdersHookHandler;
use Tygh\Addons\SdPaymentTerms\HookHandlers\CartHookHandler;
use Tygh\Addons\SdPaymentTerms\HookHandlers\PaymentsHookHandler;
use Tygh\Addons\SdPaymentTerms\Payments\Service as PaymentsService;
use Tygh\Addons\SdPaymentTerms\PaymentTerm\Service as PaymentTermService;
use Tygh\Addons\SdPaymentTerms\PaymentTerm\Repository as PaymentTermRepository;
use Tygh\Addons\SdPaymentTerms\PaymentTerm\Sync as PaymentTermSync;
use Tygh\Addons\SdPaymentTerms\PaymentTerm\Permission as PaymentTermPermission;
use Tygh\Addons\SdPaymentTerms\PaymentTerm\Factory as PaymentTermFactory;
use Tygh\Addons\SdPaymentTerms\PaymentTerm\Manager as PaymentTermManager;
use Tygh\Addons\SdPaymentTerms\PaymentTerm\Term\TermBuilder as PaymentTermBuilder;
use Tygh\Addons\SdPaymentTerms\PaymentTerm\Checker\Checker as PaymentTermChecker;
use Tygh\Addons\SdPaymentTerms\PaymentTerm\Payment\Factory as PaymentTermPaymentFactory;
use Tygh\Tygh;

/**
 * Class ServiceProvider is intended to register services and components
 * of the design integration add-on to the application container.
 *
 * @package Tygh\Addons\SdPaymentTerms
 */
class ServiceProvider implements ServiceProviderInterface
{
    /** @inheridoc */
    public function register(Container $app)
    {
        $app['addons.sd_payment_terms.hook_handlers.orders'] = function () {
            return new OrdersHookHandler();
        };

        $app['addons.sd_payment_terms.hook_handlers.cart'] = function () {
            return new CartHookHandler();
        };

        $app['addons.sd_payment_terms.hook_handlers.payments'] = function () {
            return new PaymentsHookHandler();
        };

        $app['addons.sd_payment_terms.payments.service'] = static function () {
            return new PaymentsService();
        };

        $app['addons.sd_payment_terms.payment_term.service'] = static function () {
            return new PaymentTermService();
        };

        $app['addons.sd_payment_terms.payment_term.repository'] = static function () {
            return new PaymentTermRepository();
        };

        $app['addons.sd_payment_terms.payment_term.sync'] = static function () {
            return new PaymentTermSync();
        };

        $app['addons.sd_payment_terms.payment_term.permission'] = static function () {
            return new PaymentTermPermission(
                self::getPaymentTermRepository()
            );
        };

        $app['addons.sd_payment_terms.payment_term.factory'] = static function () {
            return new PaymentTermFactory(
                fn_get_schema('sd_payment_terms', 'payment_term_factory')
            );
        };

        $app['addons.sd_payment_terms.payment_term.builder'] = $app->factory(function () {
            return new PaymentTermBuilder(
                self::getPaymentTermFactory(),
                self::getPaymentTermRepository()
            );
        });

        $app['addons.sd_payment_terms.payment_term.manager'] = $app->factory(function () {
            return function ($order_id, $type) {
                return new PaymentTermManager(
                    $order_id,
                    $type,
                    self::getPaymentTermChecker($order_id),
                    self::getPaymentTermBuilder(),
                    self::getPaymentTermFactory(),
                    self::getPaymentTermRepository()
                );
            };
        });

        $app['addons.sd_payment_terms.payment_term.checker'] = $app->factory(function () {
            return function ($order_id) {
                return new PaymentTermChecker(
                    $order_id,
                    self::getPaymentTermFactory(),
                    self::getPaymentTermRepository()
                );
            };
        });

        $app['addons.sd_payment_terms.payment_term.payment.factory'] = static function () {
            return new PaymentTermPaymentFactory();
        };
    }

    /** @return \Tygh\Addons\SdPaymentTerms\Payments\Service */
    public static function getPaymentsService()
    {
        return Tygh::$app['addons.sd_payment_terms.payments.service'];
    }

    /** @return \Tygh\Addons\SdPaymentTerms\PaymentTerm\Service */
    public static function getPaymentTermService()
    {
        return Tygh::$app['addons.sd_payment_terms.payment_term.service'];
    }

    /** @return \Tygh\Addons\SdPaymentTerms\PaymentTerm\Repository */
    public static function getPaymentTermRepository()
    {
        return Tygh::$app['addons.sd_payment_terms.payment_term.repository'];
    }

    /** @return \Tygh\Addons\SdPaymentTerms\PaymentTerm\Sync */
    public static function getPaymentTermSync()
    {
        return Tygh::$app['addons.sd_payment_terms.payment_term.sync'];
    }

    /** @return \Tygh\Addons\SdPaymentTerms\PaymentTerm\Permission */
    public static function getPaymentTermPermission()
    {
        return Tygh::$app['addons.sd_payment_terms.payment_term.permission'];
    }

    /** @return \Tygh\Addons\SdPaymentTerms\PaymentTerm\Factory */
    public static function getPaymentTermFactory()
    {
        return Tygh::$app['addons.sd_payment_terms.payment_term.factory'];
    }

    /** @return \Tygh\Addons\SdPaymentTerms\PaymentTerm\Term\TermBuilder */
    public static function getPaymentTermBuilder()
    {
        return Tygh::$app['addons.sd_payment_terms.payment_term.builder'];
    }

    /** @return \Tygh\Addons\SdPaymentTerms\PaymentTerm\Manager */
    public static function getPaymentTermManager(int $order_id, string $type)
    {
        return Tygh::$app['addons.sd_payment_terms.payment_term.manager']($order_id, $type);
    }

    /** @return \Tygh\Addons\SdPaymentTerms\PaymentTerm\Checker\Checker */
    public static function getPaymentTermChecker(int $order_id)
    {
        return Tygh::$app['addons.sd_payment_terms.payment_term.checker']($order_id);
    }

    /** @return \Tygh\Addons\SdPaymentTerms\PaymentTerm\Payment\Factory */
    public static function getPaymentTermPaymentFactory()
    {
        return Tygh::$app['addons.sd_payment_terms.payment_term.payment.factory'];
    }
}
