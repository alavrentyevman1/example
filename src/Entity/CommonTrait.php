<?php
/***************************************************************************
 *                                                                          *
 *   © Simtech Development Ltd.                                             *
 *                                                                          *
 * This  is  commercial  software,  only  users  who have purchased a valid *
 * license  and  accept  to the terms of the  License Agreement can install *
 * and use this program.                                                    *
 ***************************************************************************/

namespace Tygh\Addons\SdPaymentTerms\Entity;

use Tygh\Registry;

trait CommonTrait
{
    public function filterFillableFields(array $item_data): array
    {
        if (!empty(Registry::ifGet('payment_term.skip_filter_fillable_fields', false))) {
            return $item_data;
        }

        foreach ($item_data as $fields_key => $field_value) {
            if (!in_array($fields_key, $this->fillable)) {
                unset($item_data[$fields_key]);
            }
        }

        return $item_data;
    }
}
