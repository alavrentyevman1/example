<?php
/***************************************************************************
 *                                                                          *
 *   © Simtech Development Ltd.                                             *
 *                                                                          *
 * This  is  commercial  software,  only  users  who have purchased a valid *
 * license  and  accept  to the terms of the  License Agreement can install *
 * and use this program.                                                    *
 ***************************************************************************/

namespace Tygh\Addons\SdPaymentTerms\Entity;

use Tygh\Addons\SdPaymentTerms\ServiceProvider;
use Tygh\Addons\SdShippingMethods\Batches\OrderIdMap as BatchesOrderIdMap;

class Cache
{
    private static $orders = [];

    public static function getOrderInfo(int $order_id, bool $merge_payment_term = true): array
    {
        if (empty(self::$orders[$order_id])) {
            self::setOrderInfo($order_id, (array) fn_get_order_info($order_id));

            if ($merge_payment_term) {
                self::mergePaymentTermOrder($order_id);
            }
        }

        return self::$orders[$order_id] ?? [];
    }

    public static function setOrderInfo(int $order_id, array $order_info): void
    {
        if ($order_id) {
            self::$orders[$order_id] = $order_info;
        }
    }

    public static function removeOrderInfo(int $order_id): void
    {
        unset(self::$orders[$order_id]);
    }

    protected static function mergePaymentTermOrder(int $order_id): void
    {
        if (empty(self::$orders[$order_id])) {
            return;
        }

        $orders = ServiceProvider::getPaymentTermRepository()->getShortOrdersInfo(
            BatchesOrderIdMap::getForCurrentOrder($order_id, BatchesOrderIdMap::BY_DEALER_TYPE, true)
        );

        foreach ($orders as $order) {
            self::$orders[$order_id]['total'] += $order['total'];
            self::$orders[$order_id]['subtotal'] += $order['subtotal'];
            self::$orders[$order_id]['shipping_cost'] += $order['shipping_cost'];
            self::$orders[$order_id]['subtotal_discount'] += $order['subtotal_discount'];
            self::$orders[$order_id]['products'] += $order['products'];
            self::$orders[$order_id]['taxes'] += $order['taxes'];
        }
    }
}
