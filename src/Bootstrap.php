<?php
/***************************************************************************
 *                                                                          *
 *   © Simtech Development Ltd.                                             *
 *                                                                          *
 * This  is  commercial  software,  only  users  who have purchased a valid *
 * license  and  accept  to the terms of the  License Agreement can install *
 * and use this program.                                                    *
 ***************************************************************************/

namespace Tygh\Addons\SdPaymentTerms;

use Tygh\Core\ApplicationInterface;
use Tygh\Core\BootstrapInterface;
use Tygh\Core\HookHandlerProviderInterface;

/**
 * This class describes instructions for loading
 *
 * @package Tygh\Addons\SdPaymentTerms
 */
class Bootstrap implements BootstrapInterface, HookHandlerProviderInterface
{
    /** @inheridoc */
    public function boot(ApplicationInterface $app)
    {
        $app->register(new ServiceProvider());
    }

    /** @inheridoc */
    public function getHookHandlerMap(): array
    {
        return [
            'place_order_post' => ['addons.sd_payment_terms.hook_handlers.orders', 'onAfterPlaceOrder'],
            'delete_order' => ['addons.sd_payment_terms.hook_handlers.orders', 'onBeforeDeleteOrder'],
            'pre_get_orders' => ['addons.sd_payment_terms.hook_handlers.orders', 'onBeforeGetOrders'],
            'get_orders_post' => ['addons.sd_payment_terms.hook_handlers.orders', 'onAfterGetOrders'],

            'calculate_cart_items' => ['addons.sd_payment_terms.hook_handlers.cart', 'onBeforeCalculateCartItems'],

            'prepare_checkout_payment_methods_before_get_payments' => ['addons.sd_payment_terms.hook_handlers.payments', 'onBeforeGettingPaymentMethodsOnCheckout'],
        ];
    }
}
