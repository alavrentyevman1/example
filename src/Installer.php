<?php
/***************************************************************************
 *                                                                          *
 *   © Simtech Development Ltd.                                             *
 *                                                                          *
 * This  is  commercial  software,  only  users  who have purchased a valid *
 * license  and  accept  to the terms of the  License Agreement can install *
 * and use this program.                                                    *
 ***************************************************************************/

namespace Tygh\Addons\SdPaymentTerms;

use Tygh\Addons\InstallerInterface;
use Tygh\Core\ApplicationInterface;
use Tygh\Enum\UserTypes;

class Installer implements InstallerInterface
{
    /** @inheritDoc */
    public static function factory(ApplicationInterface $app)
    {
        return new self();
    }

    /** @inheritDoc */
    public function onBeforeInstall() {}

    /** @inheritDoc */
    public function onInstall()
    {
        fn_update_notification_receiver_search_conditions(
            'group',
            'payment_terms',
            UserTypes::VENDOR,
            fn_get_default_vendor_notification_search_conditions(true)
        );
    }

    /** @inheritDoc */
    public function onUninstall()
    {
        fn_update_notification_receiver_search_conditions(
            'group',
            'payment_terms',
            UserTypes::VENDOR,
            []
        );
    }
}
