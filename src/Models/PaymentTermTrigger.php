<?php
/***************************************************************************
 *                                                                          *
 *   © Simtech Development Ltd.                                             *
 *                                                                          *
 * This  is  commercial  software,  only  users  who have purchased a valid *
 * license  and  accept  to the terms of the  License Agreement can install *
 * and use this program.                                                    *
 ***************************************************************************/

namespace Tygh\Addons\SdPaymentTerms\Models;

use Tygh\Models\Components\AModel;
use Tygh\Addons\SdShippingMethods\Enum\TransportHubTypes;

class PaymentTermTrigger extends AModel
{
    /** @inheritDoc */
    public function getTableName(): string
    {
        return '?:payment_term_triggers';
    }

    /** @inheritDoc */
    public function getPrimaryField(): string
    {
        return 'trigger_id';
    }

    /** @inheritDoc */
    public function getDescriptionTableName(): string
    {
        return '?:payment_term_trigger_descriptions';
    }

    /** @inheritDoc */
    public function getFields($params): array
    {
        return [
            '?:payment_term_triggers.*',
            '?:payment_term_trigger_descriptions.*'
        ];
    }

    /** @inheritDoc */
    public function getSearchFields(): array
    {
        return [
            'string' => [
                'status' => '?:payment_term_triggers.status',
                'transport_hub_type' => '?:payment_term_triggers.transport_hub_type'
            ],
            'text' => [
                'name' => '?:payment_term_trigger_descriptions.name'
            ],
            'in' => [
                'transport_hub_types' => '?:payment_term_triggers.transport_hub_type'
            ]
        ];
    }

    /** @inheritDoc */
    public function getSortFields(): array
    {
        return [
            'id'     => '?:payment_term_triggers.trigger_id',
            'status' => '?:payment_term_triggers.status',
            'name'   => '?:payment_term_trigger_descriptions.name'
        ];
    }

    /** @inheritDoc */
    public function beforeSave(): bool
    {
        $allow_save = true;

        if (!empty($this->transport_hub_type)) {
            $hub_type_trigger = self::model()->find(0, [
                'transport_hub_type' => $this->transport_hub_type,
                'not_ids'            => $this->id
            ]);

            if ($hub_type_trigger) {
                $allow_save = false;
            }
        }

        return $allow_save;
    }

    /** @inheritDoc */
    public function gatherAdditionalItemsData(&$items, $params): void
    {
        if (!empty($params['load_triggers'])) {
            $items = $this->loadTriggers($items);
        }
    }

    public function loadTriggers(array $items): array
    {
        $transport_hubs = TransportHubTypes::getAllWithNames();

        return array_map(function ($item) use ($transport_hubs) {
            if (isset($transport_hubs[$item['transport_hub_type']])) {
                $item['transport_hub_data'] = [
                    'name' => $transport_hubs[$item['transport_hub_type']]
                ];
            } else {
                $item['transport_hub_data'] = [];
            }

            return $item;
        }, $items);
    }
}
