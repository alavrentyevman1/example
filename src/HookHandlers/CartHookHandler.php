<?php
/***************************************************************************
 *                                                                          *
 *   © Simtech Development Ltd.                                             *
 *                                                                          *
 * This  is  commercial  software,  only  users  who have purchased a valid *
 * license  and  accept  to the terms of the  License Agreement can install *
 * and use this program.                                                    *
 ***************************************************************************/

namespace Tygh\Addons\SdPaymentTerms\HookHandlers;

use Tygh\Addons\SdPaymentTerms\ServiceProvider;
use Tygh\Addons\SdPaymentTerms\Enum\PaymentTermGroupTypes;
use Tygh\Enum\SiteArea;
use Tygh\Tygh;

class CartHookHandler
{
    /**
     * The "calculate_cart_items" hook handler.
     *
     * Actions performed:
     *  - Checks allow update payment term
     *
     * @see fn_calculate_cart_content()
     */
    public function onBeforeCalculateCartItems(&$cart)
    {
        if (SiteArea::isStorefront(AREA)) {
            $payment_term_manager = ServiceProvider::getPaymentTermManager(0, PaymentTermGroupTypes::getDefault());
            $allow_create_term = $payment_term_manager->allowCreateTerm();

            if ($allow_create_term->isSuccess()) {
                $cart['payment_id'] = ServiceProvider::getPaymentsService()->getFirstAccountPaymentId();
            }
        }
    }
}
