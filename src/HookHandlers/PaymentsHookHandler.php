<?php
/***************************************************************************
 *                                                                          *
 *   © Simtech Development Ltd.                                             *
 *                                                                          *
 * This  is  commercial  software,  only  users  who have purchased a valid *
 * license  and  accept  to the terms of the  License Agreement can install *
 * and use this program.                                                    *
 ***************************************************************************/

namespace Tygh\Addons\SdPaymentTerms\HookHandlers;

use Tygh\Addons\SdPaymentTerms\Enum\PaymentTermGroupTypes;
use Tygh\Addons\SdPaymentTerms\ServiceProvider;
use Tygh\Enum\SiteArea;
use Tygh\Tygh;

class PaymentsHookHandler
{
    /**
     * The "prepare_checkout_payment_methods_before_get_payments" hook handler.
     *
     * Actions performed:
     *  - Add notification for created more then one payment method with proccessor 'account'
     *
     * @see fn_prepare_checkout_payment_methods()
     */
    public function onBeforeGettingPaymentMethodsOnCheckout(
        $cart,
        $auth,
        $lang_code,
        $get_payment_groups,
        $payment_methods,
        &$get_payments_params
    ) {
        if (SiteArea::isStorefront(AREA)) {
            $payment_term_manager = ServiceProvider::getPaymentTermManager(0, PaymentTermGroupTypes::getDefault());
            $allow_create_term = $payment_term_manager->allowCreateTerm();

            if ($allow_create_term->isSuccess()) {
                $get_payments_params['payment_id'] = ServiceProvider::getPaymentsService()->getFirstAccountPaymentId();
            }
        }
    }
}
