<?php
/***************************************************************************
 *                                                                          *
 *   © Simtech Development Ltd.                                             *
 *                                                                          *
 * This  is  commercial  software,  only  users  who have purchased a valid *
 * license  and  accept  to the terms of the  License Agreement can install *
 * and use this program.                                                    *
 ***************************************************************************/

namespace Tygh\Addons\SdPaymentTerms\HookHandlers;

use Tygh\Addons\SdPaymentTerms\ServiceProvider;
use Tygh\Addons\SdPaymentTerms\Enum\PaymentTermGroupTypes;
use Tygh\Addons\SdShippingMethods\Batches\OrderIdMap as BatchesOrderIdMap;
use Tygh\Addons\SdShippingMethods\ServiceProvider as ShippingMethodsServiceProvider;
use Tygh\Enum\SiteArea;

class OrdersHookHandler
{
    protected $payment_term_service;

    public function __construct()
    {
        $this->payment_term_service = ServiceProvider::getPaymentTermService();
    }

    /**
     * The "place_order_post" hook handler.
     *
     * Actions performed:
     *  - Creates default order payment term
     *
     * @see fn_place_order()
     */
    public function onAfterPlaceOrder($cart, $auth, $action, $issuer_id, $parent_order_id, $order_id)
    {
        if (SiteArea::isStorefront(AREA)) {
            $order_id = ShippingMethodsServiceProvider::getBatchesService()->replaceOrderIdByBatches($order_id);

            if (!ServiceProvider::getPaymentTermRepository()->isExistsByOrderId($order_id)) {
                $payment_term = $this->payment_term_service->getDefaultOrderPaymentTermData($order_id);
                $payment_term_manager = ServiceProvider::getPaymentTermManager($order_id, PaymentTermGroupTypes::getDefault());
                $allow_save_term = $payment_term_manager->allowSaveTerm($payment_term);

                if ($allow_save_term->isSuccess()) {
                    $payment_term_manager->saveTerm($payment_term);
                    $payment_term_manager->approveTerm();
                }
            }
        }
    }

    /**
     * The "delete_order" hook handler.
     *
     * Actions performed:
     *  - Deletes order payment term
     *
     * @see fn_delete_order()
     */
    public function onBeforeDeleteOrder($order_id)
    {
        $batch_order_id = BatchesOrderIdMap::getFirstOrderId($order_id, BatchesOrderIdMap::BY_DEALER_TYPE, true);
        $payment_term_manager = ServiceProvider::getPaymentTermManager($order_id, PaymentTermGroupTypes::getDefault());

        if ($batch_order_id) {
            $payment_term_manager->cloneTermToOrder($batch_order_id);
        }

        $payment_term_manager->deleteTermFromStorage();
    }

    /**
     * The "pre_get_orders" hook handler.
     *
     * Actions performed:
     *  - Extends query fields list
     *
     * @see fn_get_orders()
     */
    public function onBeforeGetOrders($params, &$fields)
    {
        if (!empty($params['get_payment_data'])) {
            $fields[] = '?:orders.by_dealer_id';
            $fields[] = '?:orders.payment_term_status';
            $fields[] = '?:orders.payment_percent_confirmed';
        }
    }

    /**
     * The "get_orders_post" hook handler.
     *
     * Actions performed:
     *  - Calculates payment total confirmed percent
     *
     * @see fn_get_orders()
     */
    public function onAfterGetOrders($params, &$orders)
    {
        if (!empty($params['get_payment_data'])) {
            $orders = ServiceProvider::getPaymentTermService()->fillOrdersPaymentTermData($orders);
        }
    }
}
