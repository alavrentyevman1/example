<?php
/***************************************************************************
 *                                                                          *
 *   © Simtech Development Ltd.                                             *
 *                                                                          *
 * This  is  commercial  software,  only  users  who have purchased a valid *
 * license  and  accept  to the terms of the  License Agreement can install *
 * and use this program.                                                    *
 ***************************************************************************/

use Tygh\Http;

defined('BOOTSTRAP') or die('Access denied');

$schema['controllers']['payment_terms'] = [
    'permissions' => [
        Http::POST => 'manage_payment_terms',
        Http::GET  => 'view_payment_terms'
    ]
];

return $schema;
