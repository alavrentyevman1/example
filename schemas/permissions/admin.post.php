<?php
/***************************************************************************
 *                                                                          *
 *   © Simtech Development Ltd.                                             *
 *                                                                          *
 * This  is  commercial  software,  only  users  who have purchased a valid *
 * license  and  accept  to the terms of the  License Agreement can install *
 * and use this program.                                                    *
 ***************************************************************************/

use Tygh\Http;

defined('BOOTSTRAP') or die('Access denied');

$schema['payment_term_triggers'] = [
    'permissions' => [
        Http::POST => 'manage_payment_term_triggers',
        Http::GET  => 'view_payment_term_triggers'
    ]
];

$schema['payment_terms'] = [
    'permissions' => [
        Http::POST => 'manage_payment_terms',
        Http::GET  => 'view_payment_terms'
    ]
];

$schema['tools']['modes']['update_status']['param_permissions']['table']['payment_term_triggers'] = 'manage_payment_term_triggers';

return $schema;
