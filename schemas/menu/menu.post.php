<?php
/***************************************************************************
 *                                                                          *
 *   © Simtech Development Ltd.                                             *
 *                                                                          *
 * This  is  commercial  software,  only  users  who have purchased a valid *
 * license  and  accept  to the terms of the  License Agreement can install *
 * and use this program.                                                    *
 ***************************************************************************/

defined('BOOTSTRAP') or die('Access denied');

$schema['top']['administration']['items']['payment_methods'] = [
    'href'     => 'payments.manage',
    'position' => 200,
    'subitems' => [
        'payment_methods' => [
            'href'     => 'payments.manage',
            'position' => 100
        ],
        'payment_term_triggers' => [
            'href'     => 'payment_term_triggers.manage',
            'position' => 200,
            'title'   => __('sd_payment_terms.payment_terms')
        ]
    ]
];

return $schema;
