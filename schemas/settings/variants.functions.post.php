<?php
/***************************************************************************
 *                                                                          *
 *   © Simtech Development Ltd.                                             *
 *                                                                          *
 * This  is  commercial  software,  only  users  who have purchased a valid *
 * license  and  accept  to the terms of the  License Agreement can install *
 * and use this program.                                                    *
 ***************************************************************************/

use Tygh\Addons\SdPaymentTerms\Models\PaymentTermTrigger;

defined('BOOTSTRAP') or die('Access denied');

function fn_settings_variants_addons_sd_payment_terms_default_trigger_id(): array
{
    $triggers = PaymentTermTrigger::model()->findAll([
        'to_array' => true
    ]);

    return array_combine(
        array_column($triggers, 'trigger_id'),
        array_column($triggers, 'name')
    );
}
