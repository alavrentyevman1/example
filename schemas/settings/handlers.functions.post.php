<?php
/***************************************************************************
 *                                                                          *
 *   © Simtech Development Ltd.                                             *
 *                                                                          *
 * This  is  commercial  software,  only  users  who have purchased a valid *
 * license  and  accept  to the terms of the  License Agreement can install *
 * and use this program.                                                    *
 ***************************************************************************/

defined('BOOTSTRAP') or die('Access denied');

function fn_settings_handlers_sd_payment_terms_general_default_payment_days(): array
{
    return fn_sd_payment_terms_get_days_settings_handlers();
}

function fn_settings_handlers_sd_payment_terms_general_before_end_payment_notification_event_days(): array
{
    return fn_sd_payment_terms_get_days_settings_handlers();
}

function fn_sd_payment_terms_get_days_settings_handlers(): array
{
    return [
        'input_attributes' => [
            'class'       => 'cm-numeric',
            'data-m-dec'  => '0',
            'data-a-sign' => ' ' . __('sd_payment_terms.day_or_days'),
            'data-p-sign' => 's',
            'data-v-max'  => '999',
            'data-v-min'  => '0'
        ]
    ];
}
