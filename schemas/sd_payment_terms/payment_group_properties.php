<?php
/***************************************************************************
 *                                                                          *
 *   © Simtech Development Ltd.                                             *
 *                                                                          *
 * This  is  commercial  software,  only  users  who have purchased a valid *
 * license  and  accept  to the terms of the  License Agreement can install *
 * and use this program.                                                    *
 ***************************************************************************/

use Tygh\Addons\SdPaymentTerms\Enum\PaymentTermGroupTypes;

$schema = [
    PaymentTermGroupTypes::SINGLE => [
        'type'         => PaymentTermGroupTypes::SINGLE,
        'name'         => __('sd_payment_terms.group_name.single'),
        'is_multiple'  => false,
        'has_products' => false
    ],
    PaymentTermGroupTypes::PRODUCTS => [
        'type'         => PaymentTermGroupTypes::PRODUCTS,
        'name'         => __('sd_payment_terms.group_name.products'),
        'is_multiple'  => true,
        'has_products' => true
    ],
    PaymentTermGroupTypes::SHIPMENTS => [
        'type'         => PaymentTermGroupTypes::SHIPMENTS,
        'name'         => __('sd_payment_terms.group_name.shipments'),
        'is_multiple'  => false,
        'has_products' => false
    ]
];

return $schema;
