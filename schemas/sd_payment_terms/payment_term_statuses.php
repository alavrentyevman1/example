<?php
/***************************************************************************
 *                                                                          *
 *   © Simtech Development Ltd.                                             *
 *                                                                          *
 * This  is  commercial  software,  only  users  who have purchased a valid *
 * license  and  accept  to the terms of the  License Agreement can install *
 * and use this program.                                                    *
 ***************************************************************************/

use Tygh\Addons\SdPaymentTerms\Enum\PaymentTermStatuses;
use Tygh\Enum\SiteArea;

$schema = [
    PaymentTermStatuses::EMPTY => [
        'is_approved' => false,
        'allow_confirm_step_payment' => false,
        'allow_approve' => [
            SiteArea::ADMIN_PANEL => false,
            SiteArea::STOREFRONT => false
        ],
        'is_full_approve' => [
            SiteArea::ADMIN_PANEL => false,
            SiteArea::STOREFRONT => false
        ]
    ],
    PaymentTermStatuses::CUSTOMER_APPROVE => [
        'is_approved' => false,
        'allow_confirm_step_payment' => false,
        'allow_approve' => [
            SiteArea::ADMIN_PANEL => true,
            SiteArea::STOREFRONT => false
        ],
        'is_full_approve' => [
            SiteArea::ADMIN_PANEL => true,
            SiteArea::STOREFRONT => false
        ],
        'description' => [
            SiteArea::STOREFRONT => __('sd_payment_terms.waiting_seller_approve')
        ]
    ],
    PaymentTermStatuses::ADMIN_APPROVE => [
        'is_approved' => false,
        'allow_confirm_step_payment' => false,
        'allow_approve' => [
            SiteArea::ADMIN_PANEL => false,
            SiteArea::STOREFRONT => true
        ],
        'is_full_approve' => [
            SiteArea::ADMIN_PANEL => false,
            SiteArea::STOREFRONT => true
        ],
        'description' => [
            SiteArea::ADMIN_PANEL => __('sd_payment_terms.waiting_customer_approve')
        ]
    ],
    PaymentTermStatuses::FULL_APPROVE => [
        'is_approved' => true,
        'allow_confirm_step_payment' => true,
        'allow_approve'   => [
            SiteArea::ADMIN_PANEL => false,
            SiteArea::STOREFRONT => false
        ],
        'is_full_approve' => [
            SiteArea::ADMIN_PANEL => false,
            SiteArea::STOREFRONT => false
        ],
        'description' => [
            SiteArea::ADMIN_PANEL => __('sd_payment_terms.full_approve.seller'),
            SiteArea::STOREFRONT => __('sd_payment_terms.full_approve.customer'),
        ]
    ]
];

return $schema;
