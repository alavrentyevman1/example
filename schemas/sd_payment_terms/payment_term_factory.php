<?php
/***************************************************************************
 *                                                                          *
 *   © Simtech Development Ltd.                                             *
 *                                                                          *
 * This  is  commercial  software,  only  users  who have purchased a valid *
 * license  and  accept  to the terms of the  License Agreement can install *
 * and use this program.                                                    *
 ***************************************************************************/

use Tygh\Addons\SdPaymentTerms\Enum\PaymentTermGroupTypes;

$schema = [
    PaymentTermGroupTypes::SINGLE => [
        'group_class' => '\Tygh\Addons\SdPaymentTerms\PaymentTerm\Group\Groups\SingleGroup',
        'step_class'  => '\Tygh\Addons\SdPaymentTerms\PaymentTerm\Step\Steps\SingleStep'
    ],
    PaymentTermGroupTypes::PRODUCTS => [
        'group_class' => '\Tygh\Addons\SdPaymentTerms\PaymentTerm\Group\Groups\ProductsGroup',
        'step_class'  => '\Tygh\Addons\SdPaymentTerms\PaymentTerm\Step\Steps\ProductsStep'
    ],
    PaymentTermGroupTypes::SHIPMENTS => [
        'group_class' => '\Tygh\Addons\SdPaymentTerms\PaymentTerm\Group\Groups\ShipmentsGroup',
        'step_class'  => '\Tygh\Addons\SdPaymentTerms\PaymentTerm\Step\Steps\ShipmentsStep'
    ],
];

return $schema;
