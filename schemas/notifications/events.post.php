<?php
/***************************************************************************
 *                                                                          *
 *   © Simtech Development Ltd.                                             *
 *                                                                          *
 * This  is  commercial  software,  only  users  who have purchased a valid *
 * license  and  accept  to the terms of the  License Agreement can install *
 * and use this program.                                                    *
 ***************************************************************************/

use Tygh\Notifications\DataValue;
use Tygh\Notifications\Transports\Mail\MailMessageSchema;
use Tygh\Notifications\Transports\Mail\MailTransport;
use Tygh\Enum\SiteArea;
use Tygh\Enum\UserTypes;

defined('BOOTSTRAP') or die('Access denied');

require_once __DIR__ . '/events.functions.php';

$schema['payment_terms.update_order_payment_term'] = [
    'group'     => 'payment_terms',
    'name'      => [
        'template' => 'payment_terms.event.update_order_payment_term.name',
        'params'   => [],
    ],
    'receivers' => [
        UserTypes::CUSTOMER => [
            MailTransport::getId() => MailMessageSchema::create([
                'area'            => SiteArea::STOREFRONT,
                'from'            => 'default_company_orders_department',
                'to'              => DataValue::create('order_info.email'),
                'template_code'   => 'payment_term_update_order_payment_term',
                'company_id'      => DataValue::create('order_info.company_id'),
                'language_code'   => DataValue::create('order_info.lang_code', CART_LANGUAGE),
                'data_modifier'   => static function (array $data) {
                    $data['payment_term_url'] = fn_sd_payment_terms_event_get_order_payment_term_url($data['order_info']['order_id'], SiteArea::STOREFRONT);
                    $data['string_order_ids'] = fn_sd_payment_terms_event_get_batches_string_order_ids($data['order_info']['order_id']);

                    return $data;
                }
            ])
        ]
    ]
];

$schema['payment_terms.approve_order_payment_term'] = [
    'group'     => 'payment_terms',
    'name'      => [
        'template' => 'payment_terms.event.approve_order_payment_term.name',
        'params'   => [],
    ],
    'receivers' => [
        UserTypes::CUSTOMER => [
            MailTransport::getId() => MailMessageSchema::create([
                'area'            => SiteArea::STOREFRONT,
                'from'            => 'default_company_orders_department',
                'to'              => DataValue::create('order_info.email'),
                'template_code'   => 'payment_term_approve_order_payment_term',
                'company_id'      => DataValue::create('order_info.company_id'),
                'language_code'   => DataValue::create('order_info.lang_code', CART_LANGUAGE),
                'data_modifier'   => static function (array $data) {
                    $data['payment_term_url'] = fn_sd_payment_terms_event_get_order_payment_term_url($data['order_info']['order_id'], SiteArea::STOREFRONT);
                    $data['string_order_ids'] = fn_sd_payment_terms_event_get_batches_string_order_ids($data['order_info']['order_id']);

                    return $data;
                }
            ])
        ]
    ]
];

$schema['payment_terms.before_end_payment_event'] = [
    'group'     => 'payment_terms',
    'name'      => [
        'template' => 'payment_terms.event.before_end_payment_event.name',
        'params'   => [],
    ],
    'receivers' => [
        UserTypes::CUSTOMER => [
            MailTransport::getId() => MailMessageSchema::create([
                'area'            => SiteArea::STOREFRONT,
                'from'            => 'default_company_orders_department',
                'to'              => DataValue::create('step_data.email'),
                'template_code'   => 'payment_term_before_end_payment_event',
                'company_id'      => DataValue::create('step_data.company_id'),
                'language_code'   => DataValue::create('step_data.lang_code', CART_LANGUAGE),
                'data_modifier'   => static function (array $data) {
                    $data['payment_term_url'] = fn_sd_payment_terms_event_get_order_payment_term_url($data['step_data']['order_id'], SiteArea::STOREFRONT);
                    $data['step_data']['before_end_days'] = fn_sd_payment_terms_event_get_order_payment_term_step_before_days($data['step_data']);
                    $data['string_order_ids'] = fn_sd_payment_terms_event_get_batches_string_order_ids($data['step_data']['order_id']);

                    return $data;
                }
            ])
        ]
    ]
];

$schema['payment_terms.today_end_payment_event'] = [
    'group'     => 'payment_terms',
    'name'      => [
        'template' => 'payment_terms.event.today_end_payment_event.name',
        'params'   => [],
    ],
    'receivers' => [
        UserTypes::CUSTOMER => [
            MailTransport::getId() => MailMessageSchema::create([
                'area'            => SiteArea::STOREFRONT,
                'from'            => 'default_company_orders_department',
                'to'              => DataValue::create('step_data.email'),
                'template_code'   => 'payment_term_today_end_payment_event',
                'company_id'      => DataValue::create('step_data.company_id'),
                'language_code'   => DataValue::create('step_data.lang_code', CART_LANGUAGE),
                'data_modifier'   => static function (array $data) {
                    $data['payment_term_url'] = fn_sd_payment_terms_event_get_order_payment_term_url($data['step_data']['order_id'], SiteArea::STOREFRONT);
                    $data['string_order_ids'] = fn_sd_payment_terms_event_get_batches_string_order_ids($data['step_data']['order_id']);

                    return $data;
                }
            ])
        ]
    ]
];

$schema['payment_terms.after_end_payment_event'] = [
    'group'     => 'payment_terms',
    'name'      => [
        'template' => 'payment_terms.event.after_end_payment_event.name',
        'params'   => [],
    ],
    'receivers' => [
        UserTypes::CUSTOMER => [
            MailTransport::getId() => MailMessageSchema::create([
                'area'            => SiteArea::STOREFRONT,
                'from'            => 'default_company_orders_department',
                'to'              => DataValue::create('step_data.email'),
                'template_code'   => 'payment_term_after_end_payment_event',
                'company_id'      => DataValue::create('step_data.company_id'),
                'language_code'   => DataValue::create('step_data.lang_code', CART_LANGUAGE),
                'data_modifier'   => static function (array $data) {
                    $data['payment_term_url'] = fn_sd_payment_terms_event_get_order_payment_term_url($data['step_data']['order_id'], SiteArea::STOREFRONT);
                    $data['string_order_ids'] = fn_sd_payment_terms_event_get_batches_string_order_ids($data['step_data']['order_id']);

                    return $data;
                }
            ])
        ]
    ]
];

return $schema;
