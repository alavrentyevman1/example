<?php
/***************************************************************************
 *                                                                          *
 *   © Simtech Development Ltd.                                             *
 *                                                                          *
 * This  is  commercial  software,  only  users  who have purchased a valid *
 * license  and  accept  to the terms of the  License Agreement can install *
 * and use this program.                                                    *
 ***************************************************************************/

use Tygh\Addons\SdPaymentTerms\ServiceProvider;
use Tygh\Addons\SdShippingMethods\Batches\OrderIdMap as BatchesOrderIdMap;

defined('BOOTSTRAP') or die('Access denied');

function fn_sd_payment_terms_event_get_order_payment_term_url(int $order_id, string $area): string
{
    return fn_url("orders.details?order_id={$order_id}&selected_section=payment_terms", $area);
}

function fn_sd_payment_terms_event_get_order_payment_term_step_before_days(array $step_data): string
{
    $factory = ServiceProvider::getPaymentTermFactory();

    $step = $factory->createStep($step_data['group_type'], $step_data, ['setPaymentData']);
    $step_data = $step->toArray();
    $before_end_days = ceil($step_data['payment_data']['before_end_seconds'] / SECONDS_IN_DAY);

    return (int) $before_end_days;
}

function fn_sd_payment_terms_event_get_batches_string_order_ids(int $order_id): string
{
    $batches_data = BatchesOrderIdMap::buildBatchesOrderIdsData(
        BatchesOrderIdMap::getForCurrentOrder($order_id, BatchesOrderIdMap::BY_DEALER_TYPE)
    );

    return $batches_data['string_order_ids'];
}
